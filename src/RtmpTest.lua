-- RtmpTest
-- Author:LuatTest
-- CreateDate:20210526
-- UpdateDate:20210812

module(..., package.seeall)

-- 需要带rtmp功能固件
local tag = "RtmpTest"

local function rtmpMsgHandle(msg)
	--[[
		result_code：
			0  ==  播放成功
			1  ==  播放失败
			2  ==  停止成功
			3  ==  停止失败
			4  ==  接收超时
			5  ==  连接失败
	]]
	for k, v in pairs(msg) do
		log.info(tag .. ".rtmpMsgHandle." .. k, v)
	end
    if msg.result_code == 0 then
		log.info(tag, "播放SUCCESS")
		sys.publish("RTMP_OPEN_SUCCESS")
	end
	if msg.result_code == 1 then
		log.info(tag, "播放FAIL")
	end
	if msg.result_code == 2 then
		log.info(tag, "停止SUCCESS")
	end
	if msg.result_code == 3 then
		log.info(tag, "停止FAIL")
	end
	if msg.result_code == 4 then
		log.info(tag, "接收超时")
	end
	if msg.result_code == 5 then
		log.info(tag, "连接FAIL")
	end
	if msg.result_code == 2 then
		sys.publish("RTMP_STOP_SUCCESS")
	end
end

rtos.on(rtos.MSG_RTMP, rtmpMsgHandle)

sys.taskInit(function()
	sys.waitUntil("IP_READY_IND")
	sys.wait(5000)
	audio.setVolume(1)
	audio.setChannel(2)
	log.info(tag, "网络连接成功，开始测试")
    while true do
		--[[
			功能：打开rtmp播放
			参数：rtmp的链接地址
			返回：成功为1，失败为0
		]]
		if audiocore.rtmpopen("rtmp://wiki.airm2m.com:41935/live/test") == 0 then
			log.error(tag .. ".open", "FAIL")
			continue
		end
		sys.waitUntil("RTMP_OPEN_SUCCESS")
		log.info(tag .. ".open", "SUCCESS")
		sys.wait(20000)
		
		audiocore.rtmpclose()
		sys.waitUntil("RTMP_STOP_SUCCESS")
		log.info(tag .. ".close", "SUCCESS")
    end
end)