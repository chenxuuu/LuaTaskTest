-- DispTest
-- Author:LuatTest
-- CreateDate:20200719
-- UpdateDate:20201025
module(..., package.seeall)

local waitTime1 = 2000
local waitTime2 = 5000

-- 屏幕驱动文件管理
-- require "color_lcd_spi_ILI9341"
-- require "color_lcd_spi_gc9106l"
-- require "color_lcd_spi_gc9306X"
-- require "color_lcd_spi_st7735"
-- require "color_lcd_spi_st7735S"
-- require "lvgl_icool_mipi"

if LuaTaskTestConfig.dispTest.pngConvertTest == true then
    sys.taskInit(
        function ()
            local tag = "DispTest.convertTest"
            local res, fileMD5, stringMD5
            while true do
                sys.wait(5000)
                res = disp.img.convert("/lua/logo_color.png", disp.IMG_FORMAT_PNG, "/convertData.txt", disp.IMG_FORMAT_RGB565)
                if res == 0 then
                    log.info(tag .. ".convertToFile", "SUCCESS")
                    fileMD5 = crypto.md5("/convertData.txt", "file")
                elseif res == -1 then
                    log.error(tag .. ".convertToFile", "FAIL")
                else
                    log.error(tag .. ".convertToFile", "未知错误", res)
                end
                res = disp.img.convert("/lua/logo_color.png", disp.IMG_FORMAT_PNG, nil, disp.IMG_FORMAT_RGB565)
                if res == nil then
                    log.error(tag .. ".convertToHex", "FAIL")
                else
                    stringMD5 = crypto.md5(res, string.len(res))
                    if stringMD5 == fileMD5 then
                        log.info(tag .. "convertToHex", "SUCCESS")
                    else
                        log.error(tag .. "convertToHex", "FAIL,MD5不同")
                    end
                end
            end
        end
    )
end

local gc0310_sdr = {
    zbar_scan = 1,
    i2c_addr = 0x21,
    sensor_width = 320,
    sensor_height = 240,
    id_reg = 0xf1,
    id_value = 0x10,
    spi_mode = disp.CAMERA_SPI_MODE_LINE2,
    spi_speed = disp.CAMERA_SPEED_SDR,
    spi_yuv_out = disp.CAMERA_SPI_OUT_Y1_V0_Y0_U0,

    init_cmd = {

        0xfe, 0xf0, 0xfe, 0xf0, 0xfe, 0x00, 0xfc, 0x16, -- 4e 
        0xfc, 0x16, -- 4e -- [0]apwd [6]regf_clk_gate 
        0xf2, 0x07, -- sync output
        0xf3, 0x83, -- ff--1f--01 data output
        0xf5, 0x07, -- sck_dely
        0xf7, 0x88, -- f8/   88
        0xf8, 0x00, -- 00
        0xf9, 0x4f, -- 0f--01   4d
        0xfa, 0x32, -- 32
        0xfc, 0xce, 0xfd, 0x00,
        ------------------------------------------------/
        ----------------/   CISCTL reg  ----------------/
        ------------------------------------------------/
        0x00, 0x2f, 0x01, 0x0f, 0x02, 0x04, 0x03, 0x02, 0x04, 0x12, 0x09, 0x00,
        0x0a, 0x00, 0x0b, 0x00, 0x0c, 0x02, -- 04 
        0x0d, 0x01, 0x0e, 0xec, -- e8 
        0x0f, 0x02, 0x10, 0x88, 0x16, 0x00, 0x17, 0x14, 0x18, 0x6a, -- 1a 
        0x19, 0x14, 0x1b, 0x48, 0x1c, 0x1c, 0x1e, 0x6b, 0x1f, 0x28, 0x20, 0x8b, -- 0x89 travis20140801
        0x21, 0x49, 0x22, 0xb0, 0x23, 0x04, 0x24, 0xff, 0x34, 0x20,

        ------------------------------------------------/
        --------------------   BLK   --------------------
        ------------------------------------------------/
        0x26, 0x23, 0x28, 0xff, 0x29, 0x00, 0x32, 0x00, 0x33, 0x10, 0x37, 0x20,
        0x38, 0x10, 0x47, 0x80, 0x4e, 0x0f, -- 66
        0xa8, 0x02, 0xa9, 0x80,

        ------------------------------------------------/
        ------------------   ISP reg  ------------------/
        ------------------------------------------------/
        0x40, 0xff, 0x41, 0x21, 0x42, 0xcf, 0x44, 0x02, 0x45, 0xa8, 0x46, 0x02, -- sync
        0x4a, 0x11, 0x4b, 0x01, 0x4c, 0x20, 0x4d, 0x05, 0x4f, 0x01, 0x50, 0x01,
        0x55, 0x00, 0x56, 0xf0, 0x57, 0x01, 0x58, 0x40,
        ------------------------------------------------/
        ------------------/   GAIN   --------------------
        ------------------------------------------------/
        0x70, 0x70, 0x5a, 0x84, 0x5b, 0xc9, 0x5c, 0xed, 0x77, 0x74, 0x78, 0x40,
        0x79, 0x5f, ------------------------------------------------/ 
        ------------------/   DNDD  --------------------/
        ------------------------------------------------/ 
        0x82, 0x08, -- 0x14 
        0x83, 0x0b, 0x89, 0xf0,
        ------------------------------------------------/ 
        ------------------   EEINTP  --------------------
        ------------------------------------------------/ 
        0x8f, 0xaa, 0x90, 0x8c, 0x91, 0x90, 0x92, 0x03, 0x93, 0x03, 0x94, 0x05,
        0x95, 0x43, -- 0x65
        0x96, 0xf0, ------------------------------------------------/ 
        --------------------/  ASDE  --------------------
        ------------------------------------------------/ 
        0xfe, 0x00, 0x9a, 0x20, 0x9b, 0x80, 0x9c, 0x40, 0x9d, 0x80, 0xa1, 0x30,
        0xa2, 0x32, 0xa4, 0x30, 0xa5, 0x30, 0xaa, 0x10, 0xac, 0x22,

        ------------------------------------------------/
        ------------------/   GAMMA   ------------------/
        ------------------------------------------------/
        0xfe, 0x00, 0xbf, 0x08, 0xc0, 0x1d, 0xc1, 0x34, 0xc2, 0x4b, 0xc3, 0x60,
        0xc4, 0x73, 0xc5, 0x85, 0xc6, 0x9f, 0xc7, 0xb5, 0xc8, 0xc7, 0xc9, 0xd5,
        0xca, 0xe0, 0xcb, 0xe7, 0xcc, 0xec, 0xcd, 0xf4, 0xce, 0xfa, 0xcf, 0xff,

        ------------------------------------------------/
        ------------------/   YCP  ----------------------
        ------------------------------------------------/
        0xd0, 0x40, 0xd1, 0x38, -- 0x34
        0xd2, 0x38, -- 0x34
        0xd3, 0x50, -- 0x40 
        0xd6, 0xf2, 0xd7, 0x1b, 0xd8, 0x18, 0xdd, 0x03,

        ------------------------------------------------/
        --------------------   AEC   --------------------
        ------------------------------------------------/
        0xfe, 0x01, 0x05, 0x30, 0x06, 0x75, 0x07, 0x40, 0x08, 0xb0, 0x0a, 0xc5,
        0x0b, 0x11, 0x0c, 0x00, 0x12, 0x52, 0x13, 0x38, 0x18, 0x95, 0x19, 0x96,
        0x1f, 0x20, 0x20, 0xc0, 0x3e, 0x40, 0x3f, 0x57, 0x40, 0x7d, 0x03, 0x60,

        0x44, 0x02, ------------------------------------------------/
        --------------------   AWB   --------------------
        ------------------------------------------------/
        0xfe, 0x01, 0x1c, 0x91, 0x21, 0x15, 0x50, 0x80, 0x56, 0x04, 0x59, 0x08,
        0x5b, 0x02, 0x61, 0x8d, 0x62, 0xa7, 0x63, 0xd0, 0x65, 0x06, 0x66, 0x06,
        0x67, 0x84, 0x69, 0x08, 0x6a, 0x25, 0x6b, 0x01, 0x6c, 0x00, 0x6d, 0x02,
        0x6e, 0xf0, 0x6f, 0x80, 0x76, 0x80, 0x78, 0xaf, 0x79, 0x75, 0x7a, 0x40,
        0x7b, 0x50, 0x7c, 0x0c, 0x90, 0xc9, -- stable AWB 
        0x91, 0xbe, 0x92, 0xe2, 0x93, 0xc9, 0x95, 0x1b, 0x96, 0xe2, 0x97, 0x49,
        0x98, 0x1b, 0x9a, 0x49, 0x9b, 0x1b, 0x9c, 0xc3, 0x9d, 0x49, 0x9f, 0xc7,
        0xa0, 0xc8, 0xa1, 0x00, 0xa2, 0x00, 0x86, 0x00, 0x87, 0x00, 0x88, 0x00,
        0x89, 0x00, 0xa4, 0xb9, 0xa5, 0xa0, 0xa6, 0xba, 0xa7, 0x92, 0xa9, 0xba,
        0xaa, 0x80, 0xab, 0x9d, 0xac, 0x7f, 0xae, 0xbb, 0xaf, 0x9d, 0xb0, 0xc8,
        0xb1, 0x97, 0xb3, 0xb7, 0xb4, 0x7f, 0xb5, 0x00, 0xb6, 0x00, 0x8b, 0x00,
        0x8c, 0x00, 0x8d, 0x00, 0x8e, 0x00, 0x94, 0x55, 0x99, 0xa6, 0x9e, 0xaa,
        0xa3, 0x0a, 0x8a, 0x00, 0xa8, 0x55, 0xad, 0x55, 0xb2, 0x55, 0xb7, 0x05,
        0x8f, 0x00, 0xb8, 0xcb, 0xb9, 0x9b,

        ------------------------------------------------/
        --------------------  CC ------------------------
        ------------------------------------------------/
        0xfe, 0x01, 0xd0, 0x38, -- skin red
        0xd1, 0x00, 0xd2, 0x02, 0xd3, 0x04, 0xd4, 0x38, 0xd5, 0x12, 0xd6, 0x30,
        0xd7, 0x00, 0xd8, 0x0a, 0xd9, 0x16, 0xda, 0x39, 0xdb, 0xf8,
        ------------------------------------------------/
        --------------------   LSC   --------------------
        ------------------------------------------------/
        0xfe, 0x01, 0xc1, 0x3c, 0xc2, 0x50, 0xc3, 0x00, 0xc4, 0x40, 0xc5, 0x30,
        0xc6, 0x30, 0xc7, 0x10, 0xc8, 0x00, 0xc9, 0x00, 0xdc, 0x20, 0xdd, 0x10,
        0xdf, 0x00, 0xde, 0x00,

        ------------------------------------------------/
        ------------------/  Histogram  ----------------/
        ------------------------------------------------/
        0x01, 0x10, 0x0b, 0x31, 0x0e, 0x50, 0x0f, 0x0f, 0x10, 0x6e, 0x12, 0xa0,
        0x15, 0x60, 0x16, 0x60, 0x17, 0xe0,

        ------------------------------------------------/
        --------------   Measure Window   --------------/
        ------------------------------------------------/
        0xcc, 0x0c, 0xcd, 0x10, 0xce, 0xa0, 0xcf, 0xe6,

        ------------------------------------------------/
        ----------------/   dark sun   ------------------
        ------------------------------------------------/
        0x45, 0xf7, 0x46, 0xff, 0x47, 0x15, 0x48, 0x03, 0x4f, 0x60,

        ------------------------------------------------/
        ------------------/  banding  ------------------/
        ------------------------------------------------/
        0xfe, 0x00, 0x05, 0x01, 0x06, 0x12, -- HB
        0x07, 0x00, 0x08, 0x1c, -- VB
        0xfe, 0x01, 0x25, 0x00, -- step 
        0x26, 0x1f, 0x27, 0x01, -- 6fps
        0x28, 0xf0, 0x29, 0x01, -- 6fps
        0x2a, 0xf0, 0x2b, 0x01, -- 6fps
        0x2c, 0xf0, 0x2d, 0x03, -- 3.3fps
        0x2e, 0xe0, 0x3c, 0x20,
        --------------------/  SPI   --------------------
        ------------------------------------------------/
        0xfe, 0x03, 0x01, 0x00, 0x02, 0x00, 0x10, 0x00, 0x15, 0x00, 0x17, 0x00, -- 01--03
        0x04, 0x10, -- fifo full level
        0x40, 0x00, 0x52, 0x82, -- zwb 02改成da
        0x53, 0x24, -- 24
        0x54, 0x20, 0x55, 0x20, -- QQ--01
        0x5a, 0x00, -- 00 --yuv 
        0x5b, 0x40, 0x5c, 0x01, 0x5d, 0xf0, 0x5e, 0x00, 0x51, 0x03, 0xfe, 0x00

    }
}

local WIDTH, HEIGHT, BPP = disp.getlcdinfo()
local CHAR_WIDTH = 8
local DEFAULT_WIDTH, DEFAULT_HEIGHT = 128, 160
local qrCodeWidth, data = qrencode.encode("二维码生成测试")
local WIDTH1, HEIGHT1 = 132, 162
local appid, str1, str2, str3, callback, callbackpara

function getxpos(str) return (WIDTH - string.len(str) * CHAR_WIDTH) / 2 end

function setcolor(color) if BPP ~= 1 then return disp.setcolor(color) end end

function sendFile(uartID)
    local fileHandle = io.open("/testCamera.jpg", "rb")
    if not fileHandle then
        log.error("DispTest.SendFile", "OpenFile Error")
        return
    end

    pm.wake("UART_SENT2MCU")
    uart.on(uartID, "sent", function() sys.publish("UART_SENT2MCU_OK") end)
    uart.setup(uartID, 115200, 8, uart.PAR_NONE, uart.STOP_1, nil, 1)
    while true do
        local data = fileHandle:read(1460)
        if not data then break end
        uart.write(uartID, data)
        sys.waitUntil("UART_SENT2MCU_OK")
    end

    uart.close(uartID)
    pm.sleep("UART_SENT2MCU")
    fileHandle:close()
end

local pos = {
    {24}, -- 显示1行字符串时的Y坐标
    {10, 37}, -- 显示2行字符串时，每行字符串对应的Y坐标
    {4, 24, 44} -- 显示3行字符串时，每行字符串对应的Y坐标
}

--[[
函数名：refresh
功能  ：窗口刷新处理
参数  ：无
返回值：无
]]
local function refresh()
    disp.clear()
    if str3 then disp.puttext(str3, getxpos(str3), pos[3][3]) end
    if str2 then disp.puttext(str2, getxpos(str2), pos[str3 and 3 or 2][2]) end
    if str1 then
        disp.puttext(str1, getxpos(str1),
                     pos[str3 and 3 or (str2 and 2 or 1)][1])
    end
    disp.update()
end

--[[
函数名：close
功能  ：关闭提示框窗口
参数  ：无
返回值：无
]]
local function close()
    if not appid then return end
    sys.timerStop(close)
    if callback then callback(callbackpara) end
    uiWin.remove(appid)
    appid = nil
end

-- 窗口的消息处理函数表
local app = {onUpdate = refresh}

--[[
函数名：open
功能  ：打开提示框窗口
参数  ：
        s1：string类型，显示的第1行字符串
        s2：string类型，显示的第2行字符串，可以为空或者nil
        s3：string类型，显示的第3行字符串，可以为空或者nil
        cb：function类型，提示框关闭时的回调函数，可以为nil
        cbpara：提示框关闭时回调函数的参数，可以为nil
        prd：number类型，提示框自动关闭的超时时间，单位毫秒，默认3000毫秒
返回值：无
]]
function openprompt(s1, s2, s3, cb, cbpara, prd)
    str1, str2, str3, callback, callbackpara = s1, s2, s3, cb, cbpara
    appid = uiWin.add(app)
    sys.timerStart(close, prd or 3000)
end

--[[
函数名：refresh
功能  ：窗口刷新处理
参数  ：无
返回值：无
]]
local function refresh()
    -- 清空LCD显示缓冲区
    disp.clear()
    local oldColor = setcolor(0xF100)
    disp.puttext(common.utf8ToGb2312("待机界面"),
                 getxpos(common.utf8ToGb2312("待机界面")), 0)
    local tm = misc.getClock()
    local datestr = string.format("%04d", tm.year) .. "-" ..
                        string.format("%02d", tm.month) .. "-" ..
                        string.format("%02d", tm.day)
    local timestr = string.format("%02d", tm.hour) .. ":" ..
                        string.format("%02d", tm.min)
    -- 显示日期
    setcolor(0x07E0)
    disp.puttext(datestr, getxpos(datestr), 24)
    -- 显示时间
    setcolor(0x001F)
    disp.puttext(timestr, getxpos(timestr), 44)

    -- 刷新LCD显示缓冲区到LCD屏幕上
    disp.update()
    setcolor(oldColor)
end

-- 窗口类型的消息处理函数表
local winapp = {onUpdate = refresh}

--[[
函数名：open
功能  ：打开待机界面窗口
参数  ：无
返回值：无
]]
function openidle() appid2 = uiWin.add(winapp) end

function scanCodeCb(result, codeType, codeStr)
    -- 关闭摄像头预览
    disp.camerapreviewclose()
    -- 关闭摄像头
    disp.cameraclose()
    -- 允许系统休眠
    pm.sleep("DispTest.ScanTest")
    -- 如果有LCD，显示扫描结果
    if WIDTH ~= 0 and HEIGHT ~= 0 then
        disp.clear()
        if result then
            disp.puttext(common.utf8ToGb2312("扫描成功"), 0, 5)
            disp.puttext(common.utf8ToGb2312("类型: ") .. codeType, 0, 35)
            log.info("DispTest.ScanCodeCb.CodeStr", codeStr:toHex())
            disp.puttext(common.utf8ToGb2312("结果: ") .. codeStr, 0, 65)
        else
            disp.puttext(common.utf8ToGb2312("扫描失败"), 0, 5)
        end
        disp.update()
    end
end

if LuaTaskTestConfig.dispTest.lvglTestV6 then

    local tag = "LvglTest"

    -- lcd_config
    local function init()
        local para = {
            width = 128, -- 分辨率宽度，128像素；用户根据屏的参数自行修改
            height = 160, -- 分辨率高度，160像素；用户根据屏的参数自行修改
            bpp = 16, -- 位深度，彩屏仅支持16位
            bus = lvgl.BUS_SPI4LINE, -- LCD专用SPI引脚接口，不可修改
            xoffset = 2, -- X轴偏移
            yoffset = 1, -- Y轴偏移
            freq = 13000000, -- spi时钟频率，支持110K到13M（即110000到13000000）之间的整数（包含110000和13000000）
            pinrst = pio.P0_14, -- reset，复位引脚
            pinrs = pio.P0_18, -- rs，命令/数据选择引脚
            -- 初始化命令
            -- 前两个字节表示类型：0001表示延时，0000或者0002表示命令，0003表示数据
            -- 延时类型：后两个字节表示延时时间（单位毫秒）
            -- 命令类型：后两个字节命令的值
            -- 数据类型：后两个字节数据的值
            initcmd = {
                0x00020011, 0x00010078, 0x000200B1, 0x00030002, 0x00030035,
                0x00030036, 0x000200B2, 0x00030002, 0x00030035, 0x00030036,
                0x000200B3, 0x00030002, 0x00030035, 0x00030036, 0x00030002,
                0x00030035, 0x00030036, 0x000200B4, 0x00030007, 0x000200C0,
                0x000300A2, 0x00030002, 0x00030084, 0x000200C1, 0x000300C5,
                0x000200C2, 0x0003000A, 0x00030000, 0x000200C3, 0x0003008A,
                0x0003002A, 0x000200C4, 0x0003008A, 0x000300EE, 0x000200C5,
                0x0003000E, 0x00020036, 0x000300C0, 0x000200E0, 0x00030012,
                0x0003001C, 0x00030010, 0x00030018, 0x00030033, 0x0003002C,
                0x00030025, 0x00030028, 0x00030028, 0x00030027, 0x0003002F,
                0x0003003C, 0x00030000, 0x00030003, 0x00030003, 0x00030010,
                0x000200E1, 0x00030012, 0x0003001C, 0x00030010, 0x00030018,
                0x0003002D, 0x00030028, 0x00030023, 0x00030028, 0x00030028,
                0x00030026, 0x0003002F, 0x0003003B, 0x00030000, 0x00030003,
                0x00030003, 0x00030010, 0x0002003A, 0x00030005, 0x00020029
            },
            -- 休眠命令
            sleepcmd = {0x00020010},
            -- 唤醒命令
            wakecmd = {0x00020011}
        }
        lvgl.disp_init(para)
    end

    -- 控制SPI引脚的电压域
    pmd.ldoset(15, pmd.LDO_VLCD)

    init()

    -- LCD适配
    local disp_pin = pins.setup(11, 0)

    -- LCD分辨率的宽度和高度(单位是像素)
    WIDTH, HEIGHT, BPP = lvgl.disp_get_lcd_info()
    -- 1个ASCII字符宽度为8像素，高度为16像素；汉字宽度和高度都为16像素
    CHAR_WIDTH = 8

    sys.taskInit(function() disp_pin(1) end)

    lvgl.SYMBOL_AUDIO = "\xef\x80\x81"
    lvgl.SYMBOL_VIDEO = "\xef\x80\x88"
    lvgl.SYMBOL_LIST = "\xef\x80\x8b"
    lvgl.SYMBOL_OK = "\xef\x80\x8c"
    lvgl.SYMBOL_CLOSE = "\xef\x80\x8d"
    lvgl.SYMBOL_POWER = "\xef\x80\x91"
    lvgl.SYMBOL_SETTINGS = "\xef\x80\x93"
    lvgl.SYMBOL_HOME = "\xef\x80\x95"
    lvgl.SYMBOL_DOWNLOAD = "\xef\x80\x99"
    lvgl.SYMBOL_DRIVE = "\xef\x80\x9c"
    lvgl.SYMBOL_REFRESH = "\xef\x80\xa1"
    lvgl.SYMBOL_MUTE = "\xef\x80\xa6"
    lvgl.SYMBOL_VOLUME_MID = "\xef\x80\xa7"
    lvgl.SYMBOL_VOLUME_MAX = "\xef\x80\xa8"
    lvgl.SYMBOL_IMAGE = "\xef\x80\xbe"
    lvgl.SYMBOL_EDIT = "\xef\x8C\x84"
    lvgl.SYMBOL_PREV = "\xef\x81\x88"
    lvgl.SYMBOL_PLAY = "\xef\x81\x8b"
    lvgl.SYMBOL_PAUSE = "\xef\x81\x8c"
    lvgl.SYMBOL_STOP = "\xef\x81\x8d"
    lvgl.SYMBOL_NEXT = "\xef\x81\x91"
    lvgl.SYMBOL_EJECT = "\xef\x81\x92"
    lvgl.SYMBOL_LEFT = "\xef\x81\x93"
    lvgl.SYMBOL_RIGHT = "\xef\x81\x94"
    lvgl.SYMBOL_PLUS = "\xef\x81\xa7"
    lvgl.SYMBOL_MINUS = "\xef\x81\xa8"
    lvgl.SYMBOL_EYE_OPEN = "\xef\x81\xae"
    lvgl.SYMBOL_EYE_CLOSE = "\xef\x81\xb0"
    lvgl.SYMBOL_WARNING = "\xef\x81\xb1"
    lvgl.SYMBOL_SHUFFLE = "\xef\x81\xb4"
    lvgl.SYMBOL_UP = "\xef\x81\xb7"
    lvgl.SYMBOL_DOWN = "\xef\x81\xb8"
    lvgl.SYMBOL_LOOP = "\xef\x81\xb9"
    lvgl.SYMBOL_DIRECTORY = "\xef\x81\xbb"
    lvgl.SYMBOL_UPLOAD = "\xef\x82\x93"
    lvgl.SYMBOL_CALL = "\xef\x82\x95"
    lvgl.SYMBOL_CUT = "\xef\x83\x84"
    lvgl.SYMBOL_COPY = "\xef\x83\x85"
    lvgl.SYMBOL_SAVE = "\xef\x83\x87"
    lvgl.SYMBOL_CHARGE = "\xef\x83\xa7"
    lvgl.SYMBOL_PASTE = "\xef\x83\xAA"
    lvgl.SYMBOL_BELL = "\xef\x83\xb3"
    lvgl.SYMBOL_KEYBOARD = "\xef\x84\x9c"
    lvgl.SYMBOL_GPS = "\xef\x84\xa4"
    lvgl.SYMBOL_FILE = "\xef\x85\x9b"
    lvgl.SYMBOL_WIFI = "\xef\x87\xab"
    lvgl.SYMBOL_BATTERY_FULL = "\xef\x89\x80"
    lvgl.SYMBOL_BATTERY_3 = "\xef\x89\x81"
    lvgl.SYMBOL_BATTERY_2 = "\xef\x89\x82"
    lvgl.SYMBOL_BATTERY_1 = "\xef\x89\x83"
    lvgl.SYMBOL_BATTERY_EMPTY = "\xef\x89\x84"
    lvgl.SYMBOL_USB = "\xef\x8a\x87"
    lvgl.SYMBOL_BLUETOOTH = "\xef\x8a\x93"
    lvgl.SYMBOL_TRASH = "\xef\x8B\xAD"
    lvgl.SYMBOL_BACKSPACE = "\xef\x95\x9A"
    lvgl.SYMBOL_SD_CARD = "\xef\x9F\x82"
    lvgl.SYMBOL_NEW_LINE = "\xef\xA2\xA2"

    -- page1
    function page1create()
        scr = lvgl.cont_create(nil, nil)
        cv = lvgl.canvas_create(scr, nil)
        lvgl.canvas_set_buffer(cv, 100, 100)
        lvgl.obj_align(cv, nil, lvgl.ALIGN_CENTER, 0, 0)
        layer_id = lvgl.canvas_to_disp_layer(cv)
        disp.setactlayer(layer_id)
        width, data = qrencode.encode('http://www.openluat.com')
        l_w, l_h = disp.getlayerinfo()
        displayWidth = 100
        disp.putqrcode(data, width, displayWidth, (l_w - displayWidth) / 2,
                       (l_h - displayWidth) / 2)
        disp.update()
        label = lvgl.label_create(scr, nil)
        lvgl.label_set_recolor(label, true)
        lvgl.label_set_text(label, "#008080 上海合宙")
        lvgl.obj_align(label, cv, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 2)
        return scr
    end

    -- page2
    arc = nil

    angles = 0

    local function arc_loader()
        angles = angles + 5
        if angles < 180 then
            lvgl.arc_set_angles(arc, 180 - angles, 180)
        else
            lvgl.arc_set_angles(arc, 540 - angles, 180)
        end
        if angles == 360 then angles = 0 end
    end

    function page2create()
        scr = lvgl.cont_create(nil, nil)
        style = lvgl.style_t()
        lvgl.style_copy(style, lvgl.style_plain)
        style.line.color = lvgl.color_hex(0x800000)
        style.line.width = 4

        arc = lvgl.arc_create(scr, nil)
        lvgl.arc_set_style(arc, lvgl.ARC_STYLE_MAIN, style)
        lvgl.arc_set_angles(arc, 180, 180)
        lvgl.obj_set_size(arc, 40, 40)
        lvgl.obj_align(arc, nil, lvgl.ALIGN_CENTER, -30, -30)
        arc_label = lvgl.label_create(scr, nil)
        lvgl.label_set_text(arc_label, "加载器")
        lvgl.obj_align(arc_label, arc, lvgl.ALIGN_OUT_RIGHT_MID, 4, 0)

        btn = lvgl.btn_create(scr, nil)
        btn_label = lvgl.label_create(btn, nil)
        lvgl.label_set_text(btn_label, "按钮")
        lvgl.obj_align(btn, nil, lvgl.ALIGN_CENTER, 0, 40)
        lvgl.obj_set_size(btn, 60, 60)

        sys.timerLoopStart(arc_loader, 100)

        return scr
    end

    -- page3
    btn = nil

    local function set_y(btn, value) lvgl.obj_set_y(btn, value) end

    anim = nil

    local function stop_anim()
        lvgl.anim_del(anim, set_y)
        lvgl.obj_set_y(btn, 10)
    end

    function page3create()
        theme = lvgl.theme_material_init(460, nil)
        lvgl.theme_set_current(theme)
        scr = lvgl.cont_create(nil, nil)
        btn = lvgl.btn_create(scr, nil)
        lvgl.obj_set_pos(btn, 10, 10)
        lvgl.obj_set_size(btn, 80, 50)
        label = lvgl.label_create(btn, nil)
        lvgl.label_set_text(label, "动画")
        anim = lvgl.anim_t()

        lvgl.anim_set_values(anim, -lvgl.obj_get_height(btn),
                             lvgl.obj_get_y(btn), lvgl.ANIM_PATH_OVERSHOOT)
        lvgl.anim_set_time(anim, 300, -2000)
        lvgl.anim_set_repeat(anim, 500)
        lvgl.anim_set_playback(anim, 500)
        lvgl.anim_set_exec_cb(anim, btn, set_y)
        lvgl.anim_create(anim)

        btn2 = lvgl.btn_create(scr, nil)
        lvgl.obj_set_pos(btn2, 10, 80)
        lvgl.obj_set_size(btn2, 100, 50)
        btn2_label = lvgl.label_create(btn2, nil)
        lvgl.label_set_text(btn2_label, "样式动画")

        btn2_style = lvgl.style_t()
        lvgl.style_copy(btn2_style, lvgl.btn_get_style(btn, lvgl.BTN_STYLE_REL))
        lvgl.btn_set_style(btn2, lvgl.BTN_STYLE_REL, btn2_style)
        style_anim = lvgl.anim_t()
        lvgl.style_anim_init(style_anim)
        lvgl.style_anim_set_styles(style_anim, btn2_style, lvgl.style_btn_rel,
                                   lvgl.style_pretty)
        lvgl.style_anim_set_time(style_anim, 500, 500)
        lvgl.style_anim_set_playback(style_anim, 500)
        lvgl.style_anim_set_repeat(style_anim, 500)
        lvgl.style_anim_create(style_anim)
        sys.timerStart(stop_anim, 3000)
        return scr
    end

    -- page4
    function page4create()
        black = lvgl.color_make(0, 0, 0)
        white = lvgl.color_make(0xff, 0xff, 0xff)
        scr = lvgl.cont_create(nil, nil)
        style_sb = lvgl.style_t()
        style_sb.body.main_color = black
        style_sb.body.grad_color = black
        style_sb.body.border.color = white
        style_sb.body.border.width = 1
        style_sb.body.border.opa = lvgl.OPA_70
        style_sb.body.radius = lvgl.RADIUS_CIRCLE
        style_sb.body.opa = lvgl.OPA_60
        style_sb.body.padding.right = 3
        style_sb.body.padding.bottom = 3
        style_sb.body.padding.inner = 8

        page = lvgl.page_create(scr, nil)
        lvgl.obj_set_size(page, 100, 150)
        lvgl.obj_align(page, nil, lvgl.ALIGN_CENTER, 0, 0)
        lvgl.page_set_style(page, lvgl.PAGE_STYLE_SB, style_sb)

        label = lvgl.label_create(page, nil)
        lvgl.label_set_long_mode(label, lvgl.LABEL_LONG_BREAK)
        lvgl.obj_set_width(label, lvgl.page_get_fit_width(page))
        lvgl.label_set_recolor(label, true)
        lvgl.label_set_text(label, [[
			Air722UG
			Air724UG
			行1
			行2
			行3]])
        return scr
    end

    -- page5

    function page5create()
        scr = lvgl.cont_create(nil, nil)
        list = lvgl.list_create(scr, nil)
        lvgl.obj_set_size(list, 100, 140)
        lvgl.obj_align(list, nil, lvgl.ALIGN_CENTER, 0, 0)
        lvgl.list_add_btn(list, lvgl.SYMBOL_LIST, "我是列表")
        lvgl.list_add_btn(list, lvgl.SYMBOL_OK, "确认")
        lvgl.list_add_btn(list, lvgl.SYMBOL_PAUSE, "暂停")
        return scr
    end

    -- page6

    cb = nil

    test_data = "blablabla"

    local function test_cb(cb, e)
        if e == lvgl.EVENT_CLICKED then
            lvgl.cb_set_checked(cb, true)
            print(lvgl.event_get_data())
        end
    end

    local function click() lvgl.event_send(cb, lvgl.EVENT_CLICKED, test_data) end

    function page6create()
        scr = lvgl.cont_create(nil, nil)
        cb = lvgl.cb_create(scr, nil)
        lvgl.cb_set_text(cb, "我同意")
        lvgl.obj_align(cb, nil, lvgl.ALIGN_CENTER, 0, 0)
        lvgl.obj_set_event_cb(cb, test_cb)
        sys.timerStart(click, 2000)
        return scr
    end

    -- page7

    scr2 = nil

    local function close_win(btn, event)
        if event == lvgl.EVENT_RELEASED then
            win = lvgl.win_get_from_btn(btn)
            lvgl.obj_del(win)
            lvgl.disp_load_scr(scr2)
        end
    end

    function page7create()
        scr = lvgl.cont_create(nil, nil)
        scr2 = lvgl.cont_create(nil, nil)
        win = lvgl.win_create(scr, nil)

        lvgl.win_set_title(win, "标题")

        close_btn = lvgl.win_add_btn(win, lvgl.SYMBOL_CLOSE)
        lvgl.obj_set_event_cb(close_btn, close_win)
        lvgl.win_add_btn(win, lvgl.SYMBOL_SETTINGS)

        txt = lvgl.label_create(win, nil)
        lvgl.label_set_recolor(txt, true)
        lvgl.label_set_text(txt, [[This #987654 is the# content of the window
							   You can add control buttons to
							   the window header
							   The content area becomes automatically
							   scrollable is it's large enough.
							   You can scroll the content
							   See the scroll bar on the right!]])

        ml = lvgl.label_create(scr2, nil)
        lvgl.label_set_recolor(ml, true)
        lvgl.label_set_text(ml, "#123456 窗口# #897632 已关闭#")
        lvgl.obj_align(ml, nil, lvgl.ALIGN_CENTER, 0, 0)
        sys.timerStart(lvgl.event_send, 3000, close_btn, lvgl.EVENT_RELEASED,
                       nil)
        return scr
    end

    -- page8
    function page8create()
        scr = lvgl.cont_create(nil, nil)
        style_bg = lvgl.style_t()
        style_indic = lvgl.style_t()
        style_knob = lvgl.style_t()

        lvgl.style_copy(style_bg, lvgl.style_pretty)
        style_bg.body.main_color = lvgl.color_hex(0x00ff00)
        style_bg.body.grad_color = lvgl.color_hex(0x000080)
        style_bg.body.radius = lvgl.RADIUS_CIRCLE
        style_bg.body.border.color = lvgl.color_hex(0xffffff)

        lvgl.style_copy(style_indic, lvgl.style_pretty_color)
        style_indic.body.radius = lvgl.RADIUS_CIRCLE
        style_indic.body.shadow.width = 8
        style_indic.body.shadow.color = style_indic.body.main_color
        style_indic.body.padding.left = 3
        style_indic.body.padding.right = 3
        style_indic.body.padding.top = 3
        style_indic.body.padding.bottom = 3

        lvgl.style_copy(style_knob, lvgl.style_pretty)
        style_knob.body.radius = lvgl.RADIUS_CIRCLE
        style_knob.body.opa = lvgl.OPA_70
        style_knob.body.padding.top = 10
        style_knob.body.padding.bottom = 10

        slider = lvgl.slider_create(scr, nil)
        lvgl.obj_set_size(slider, 100, 20)
        lvgl.slider_set_style(slider, lvgl.SLIDER_STYLE_BG, style_bg)
        lvgl.slider_set_style(slider, lvgl.SLIDER_STYLE_INDIC, style_indic)
        lvgl.slider_set_style(slider, lvgl.SLIDER_STYLE_KNOB, style_knob)
        lvgl.obj_align(slider, nil, lvgl.ALIGN_CENTER, 0, 0)

        label = lvgl.label_create(scr, nil)
        lvgl.label_set_text(label, "滑动条")
        lvgl.obj_align(label, slider, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 0)
        return scr
    end

    -- page9
    sw = nil

    local function sw_on() lvgl.sw_on(sw, lvgl.ANIM_ON) end

    local function sw_off() lvgl.sw_off(sw, lvgl.ANIM_ON) end

    local function sw_toggle(on)
        if on then
            sw_on()
        else
            sw_off()
        end
        sys.timerStart(sw_toggle, 1000, not on)
    end

    function page9create()
        scr = lvgl.cont_create(nil, nil)
        bg_style = lvgl.style_t()
        indic_style = lvgl.style_t()
        knob_on_style = lvgl.style_t()
        knob_off_style = lvgl.style_t()

        lvgl.style_copy(bg_style, lvgl.style_pretty)
        bg_style.body.radius = lvgl.RADIUS_CIRCLE
        bg_style.body.padding.top = 6
        bg_style.body.padding.bottom = 6

        lvgl.style_copy(indic_style, lvgl.style_pretty_color)
        indic_style.body.radius = lvgl.RADIUS_CIRCLE
        indic_style.body.main_color = lvgl.color_hex(0x9fc8ef)
        indic_style.body.grad_color = lvgl.color_hex(0x9fc8ef)
        indic_style.body.padding.left = 0
        indic_style.body.padding.right = 0
        indic_style.body.padding.top = 0
        indic_style.body.padding.bottom = 0

        lvgl.style_copy(knob_off_style, lvgl.style_pretty_color)
        knob_off_style.body.radius = lvgl.RADIUS_CIRCLE
        knob_off_style.body.shadow.width = 4
        knob_off_style.body.shadow.type = lvgl.SHADOW_BOTTOM

        lvgl.style_copy(knob_on_style, lvgl.style_pretty_color)
        knob_on_style.body.radius = lvgl.RADIUS_CIRCLE
        knob_on_style.body.shadow.width = 4
        knob_on_style.body.shadow.type = lvgl.SHADOW_BOTTOM

        sw = lvgl.sw_create(scr, nil)
        lvgl.obj_align(sw, nil, lvgl.ALIGN_CENTER, 0, 0)

        lvgl.sw_set_style(sw, lvgl.SW_STYLE_BG, bg_style)
        lvgl.sw_set_style(sw, lvgl.SW_STYLE_INDIC, indic_style)
        lvgl.sw_set_style(sw, lvgl.SW_STYLE_KNOB_ON, knob_on_style)
        lvgl.sw_set_style(sw, lvgl.SW_STYLE_KNOB_OFF, knob_off_style)

        label = lvgl.label_create(scr, nil)
        lvgl.label_set_text(label, "开关")
        lvgl.obj_align(label, sw, lvgl.ALIGN_OUT_BOTTOM_MID, 0, 2)
        sys.timerStart(sw_toggle, 1000, true)
        return scr

    end

    scrs = {
        page1create, page2create, page3create, page4create, page5create,
        page6create, page7create, page8create, page9create
    }
    obj = {}
    local function empty()
        c = lvgl.cont_create(nil, nil)
        img = lvgl.img_create(c, nil)
        lvgl.img_set_src(img, "/lua/logo_color.png")
        lvgl.obj_align(img, nil, lvgl.ALIGN_CENTER, 0, 0)
        lvgl.disp_load_scr(c)
    end

    sys.taskInit(function()
        local count = 1
        lvgl.init(empty, nil)
        sys.wait(1000)
        for k, v in ipairs(scrs) do
            obj[#obj + 1] = v()
            sys.wait(1000)
        end

        while true do
            log.info(tag, "第" .. count .. "次")
            for i = 1, #obj do
                lvgl.disp_load_scr(obj[i])
                sys.wait(5000)
            end
            count = count + 1
        end
    end)
elseif LuaTaskTestConfig.dispTest.lvglTestV7_icool then

    require "tp"
    require "DemoStyle"
    
    lvgl.SYMBOL_AUDIO="\xef\x80\x81"
    lvgl.SYMBOL_VIDEO="\xef\x80\x88"
    lvgl.SYMBOL_LIST="\xef\x80\x8b"
    lvgl.SYMBOL_OK="\xef\x80\x8c"
    lvgl.SYMBOL_CLOSE="\xef\x80\x8d"
    lvgl.SYMBOL_POWER="\xef\x80\x91"
    lvgl.SYMBOL_SETTINGS="\xef\x80\x93"
    lvgl.SYMBOL_HOME="\xef\x80\x95"
    lvgl.SYMBOL_DOWNLOAD="\xef\x80\x99"
    lvgl.SYMBOL_DRIVE="\xef\x80\x9c"
    lvgl.SYMBOL_REFRESH="\xef\x80\xa1"
    lvgl.SYMBOL_MUTE="\xef\x80\xa6"
    lvgl.SYMBOL_VOLUME_MID="\xef\x80\xa7"
    lvgl.SYMBOL_VOLUME_MAX="\xef\x80\xa8"
    lvgl.SYMBOL_IMAGE="\xef\x80\xbe"
    lvgl.SYMBOL_EDIT="\xef\x8C\x84"
    lvgl.SYMBOL_PREV="\xef\x81\x88"
    lvgl.SYMBOL_PLAY="\xef\x81\x8b"
    lvgl.SYMBOL_PAUSE="\xef\x81\x8c"
    lvgl.SYMBOL_STOP="\xef\x81\x8d"
    lvgl.SYMBOL_NEXT="\xef\x81\x91"
    lvgl.SYMBOL_EJECT="\xef\x81\x92"
    lvgl.SYMBOL_LEFT="\xef\x81\x93"
    lvgl.SYMBOL_RIGHT="\xef\x81\x94"
    lvgl.SYMBOL_PLUS="\xef\x81\xa7"
    lvgl.SYMBOL_MINUS="\xef\x81\xa8"
    lvgl.SYMBOL_EYE_OPEN="\xef\x81\xae"
    lvgl.SYMBOL_EYE_CLOSE="\xef\x81\xb0"
    lvgl.SYMBOL_WARNING="\xef\x81\xb1"
    lvgl.SYMBOL_SHUFFLE="\xef\x81\xb4"
    lvgl.SYMBOL_UP="\xef\x81\xb7"
    lvgl.SYMBOL_DOWN="\xef\x81\xb8"
    lvgl.SYMBOL_LOOP="\xef\x81\xb9"
    lvgl.SYMBOL_DIRECTORY="\xef\x81\xbb"
    lvgl.SYMBOL_UPLOAD="\xef\x82\x93"
    lvgl.SYMBOL_CALL="\xef\x82\x95"
    lvgl.SYMBOL_CUT="\xef\x83\x84"
    lvgl.SYMBOL_COPY="\xef\x83\x85"
    lvgl.SYMBOL_SAVE="\xef\x83\x87"
    lvgl.SYMBOL_CHARGE="\xef\x83\xa7"
    lvgl.SYMBOL_PASTE="\xef\x83\xAA"
    lvgl.SYMBOL_BELL="\xef\x83\xb3"
    lvgl.SYMBOL_KEYBOARD="\xef\x84\x9c"
    lvgl.SYMBOL_GPS="\xef\x84\xa4"
    lvgl.SYMBOL_FILE="\xef\x85\x9b"
    lvgl.SYMBOL_WIFI="\xef\x87\xab"
    lvgl.SYMBOL_BATTERY_FULL="\xef\x89\x80"
    lvgl.SYMBOL_BATTERY_3="\xef\x89\x81"
    lvgl.SYMBOL_BATTERY_2="\xef\x89\x82"
    lvgl.SYMBOL_BATTERY_1="\xef\x89\x83"
    lvgl.SYMBOL_BATTERY_EMPTY="\xef\x89\x84"
    lvgl.SYMBOL_USB="\xef\x8a\x87"
    lvgl.SYMBOL_BLUETOOTH="\xef\x8a\x93"
    lvgl.SYMBOL_TRASH="\xef\x8B\xAD"
    lvgl.SYMBOL_BACKSPACE="\xef\x95\x9A"
    lvgl.SYMBOL_SD_CARD="\xef\x9F\x82"
    lvgl.SYMBOL_NEW_LINE="\xef\xA2\xA2"
    
    function demo_ArcInit()
        --创建一个 Arc
        demo_Arc = lvgl.arc_create(lvgl.scr_act(), nil)
        --设置 Arc 的大小
        lvgl.obj_set_size(demo_Arc, 200, 200)
        --设置 Arc 的位置
        lvgl.obj_align(demo_Arc, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_Arc, lvgl.ARC_PART_BG, demo_ThemeStyle_Bg)
        --设置 Arc 的旋转角度
        lvgl.arc_set_angles(demo_Arc, 0, 360)
        lvgl.arc_set_end_angle(demo_Arc, 200)
    end

    function demo_LabelInit()
        --创建一个 Label
        demo_Label = lvgl.label_create(lvgl.scr_act(), nil)
        --设置 Label 的长屏滚动显示
        lvgl.label_set_long_mode(demo_Label, lvgl.LABEL_LONG_SROLL_CIRC)
        --设置 Label 的显示长度(若设置了显示方式，则需要在其后设置才会有效)
        lvgl.obj_set_width(demo_Label, 250)
        --设置 Label 的内容
        lvgl.label_set_text(demo_Label, "To be or not to be, That's a question!")
        --设置 Label 的位置
        lvgl.obj_align(demo_Label, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_Label, lvgl.LABEL_PART_MAIN, demo_ThemeFontStyle)
    end

    function demo_LEDlInit()
        --创建一个 LED1
        demo_LED1 = lvgl.led_create(lvgl.scr_act(), nil)
        lvgl.obj_set_size(demo_LED1, 80, 80)
        --关闭 LED
        lvgl.led_off(demo_LED1)
        --设置 LED 的位置
        lvgl.obj_align(demo_LED1, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, -140)
    --------------------------------------------------------------------------------------
        --创建一个 LED2
        demo_LED2 = lvgl.led_create(lvgl.scr_act(), nil)
        lvgl.obj_set_size(demo_LED2, 80, 80)
        --设置 LED 的亮度
        lvgl.led_set_bright(demo_LED2, 200)
        --设置 LED 的位置
        lvgl.obj_align(demo_LED2, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
    --------------------------------------------------------------------------------------
        --创建一个 LED3
        demo_LED3 = lvgl.led_create(lvgl.scr_act(), nil)
        lvgl.obj_set_size(demo_LED3, 80, 80)
        --打开 LED
        lvgl.led_on(demo_LED3)
        --设置 LED 的位置
        lvgl.obj_align(demo_LED3, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 140)
        --添加样式
        lvgl.obj_add_style(demo_LED1, lvgl.LED_PART_MAIN, demo_LEDStyle_Red)
        lvgl.obj_add_style(demo_LED2, lvgl.LED_PART_MAIN, demo_LEDStyle_Yellow)
        lvgl.obj_add_style(demo_LED3, lvgl.LED_PART_MAIN, demo_LEDStyle_Green)
    end

    function demo_BarInit()
        --创建一个 Bar
        demo_Bar = lvgl.bar_create(lvgl.scr_act(), nil)
        --设置 Bar 的大小
        lvgl.obj_set_size(demo_Bar, 400, 40)
        --设置 Bar 的位置
        lvgl.obj_align(demo_Bar, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --设置动画时间
        lvgl.bar_set_anim_time(demo_Bar, 6000)
        --为 Bar 添加动画
        lvgl.bar_set_value(demo_Bar, 100, lvgl.ANIM_ON)
        --添加样式
        lvgl.obj_add_style(demo_Bar, lvgl.BAR_PART_INDIC, demo_ThemeStyle_IndicAndFont)
        lvgl.obj_add_style(demo_Bar, lvgl.BTN_PART_MAIN, demo_ThemeStyle_Bg)
    end
 
    function demo_BtnInit()
        --创建一个 Button
        demo_Btn = lvgl.btn_create(lvgl.scr_act(), nil)
        --设置 Button 的大小
        lvgl.obj_set_size(demo_Btn, 200, 80)
        --设置 Button 的位置
        lvgl.obj_align(demo_Btn, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_Btn, lvgl.BTN_PART_MAIN, demo_ThemeStyle_IndicAndFont)
        lvgl.obj_add_style(demo_Btn, lvgl.BTN_PART_MAIN, demo_ThemeStyle_IndicAndFont)
    end

    function demo_CalendarInit()
        --创建一个 Calendar
        demo_Calendar = lvgl.calendar_create(lvgl.scr_act(), nil)
        --设置 Calendar 的大小
        lvgl.obj_set_size(demo_Calendar, 440, 320)
        --设置 Calendar 的位置
        lvgl.obj_align(demo_Calendar, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_Calendar, lvgl.CALENDAR_PART_BG, demo_ThemeStyle_Bg)
        --设置当前的日期
        demo_today = lvgl.calendar_date_t()
        demo_today.year = 2030
        demo_today.month = 10
        demo_today.day = 1
        lvgl.calendar_set_today_date(demo_Calendar, demo_today)
        lvgl.calendar_set_showed_date(demo_Calendar, demo_today)
        --设置高亮的日期
        demo_highlightDate = lvgl.calendar_date_t()
        demo_highlightDate.year = 2030
        demo_highlightDate.month = 10
        demo_highlightDate.day = 16
        lvgl.calendar_set_highlighted_dates(demo_Calendar, demo_highlightDate, 1)
    end

    function demo_ChartInit()
        --创建一个 Chart
        demo_Chart = lvgl.chart_create(lvgl.scr_act(), nil)
        --设置 Chart 的大小
        lvgl.obj_set_size(demo_Chart, 460, 400)
        --设置 CheckBox 的位置
        lvgl.obj_align(demo_Chart, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_Chart, lvgl.CHART_PART_BG, demo_ThemeStyle_Bg)
    
        --显示模式只选其一
        --设置 Chart 的显示模式(线表图)
        -- lvgl.chart_set_type(demo_Chart, lvgl.CHART_TYPE_LINE)
        --设置 Chart 的显示模式(柱状图)
        lvgl.chart_set_type(demo_Chart, lvgl.CHART_TYPE_COLUMN)
    
        --添加点线颜色
        Demo_Chart_Blue = lvgl.chart_add_series(demo_Chart, lvgl.color_hex(0x0000FF))
        Demo_Chart_Red = lvgl.chart_add_series(demo_Chart, lvgl.color_hex(0xFF0000))
    
        --添加蓝色点数据
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Blue, 20)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Blue, 20)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Blue, 20)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Blue, 20)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Blue, 30)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Blue, 40)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Blue, 60)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Blue, 60)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Blue, 55)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Blue, 50)
        --添加红色点数据
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Red, 70)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Red, 60)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Red, 60)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Red, 50)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Red, 40)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Red, 35)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Red, 30)
        lvgl.chart_set_next(demo_Chart, Demo_Chart_Red, 30)
    
    end

    function demo_CheckBoxInit()
        --创建一个 CheckBox
        demo_CheckBox = lvgl.checkbox_create(lvgl.scr_act(), nil)
        --设置 CheckBox 的说明文字
        lvgl.checkbox_set_text(demo_CheckBox, "This is a CheckBox")
        --设置 CheckBox 的位置
        lvgl.obj_align(demo_CheckBox, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_CheckBox, lvgl.CHECKBOX_PART_BG, demo_ThemeFontStyle)
        --CheckBox 无法设置大小
        --随着其中的说明文字的大小而自动改变大小
    end

    function demo_CPickerInit()
        --创建一个 ColorPicker
        demo_CPicker = lvgl.cpicker_create(lvgl.scr_act(), nil)
        --设置 ColorPicker 的大小
        lvgl.obj_set_size(demo_CPicker, 380, 380)
        --设置 ColorPicker 的位置
        lvgl.obj_align(demo_CPicker, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_CPicker, lvgl.CPICKER_PART_MAIN, demo_ThemeStyle_Bg)
    end

    function demo_ContInit()
        --创建一个 Cont
        demo_Cont = lvgl.cont_create(lvgl.scr_act(), nil)
        --设置 Cont 的大小
        lvgl.obj_set_size(demo_Cont, 460, 600)
        --设置 Cont 的位置
        lvgl.obj_align(demo_Cont, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_Cont, lvgl.CONT_PART_MAIN, demo_ThemeStyle_Bg)
    end

    function demo_DropDownInit()
        --创建一个 DropDown
        demo_DropDown = lvgl.dropdown_create(lvgl.scr_act(), nil)
        --添加DropDown的选项
        lvgl.dropdown_set_options(demo_DropDown, "东皇钟\n轩辕剑\n盘古斧\n炼妖壶\n昊天塔\n伏羲琴\n神农鼎\n崆峒印\n昆仑镜\n女娲石")
        --设置 DropDown 的位置
        lvgl.obj_align(demo_DropDown, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, -100, -200)
        --设置 DropDown 的下拉列表方向
        lvgl.dropdown_set_dir(demo_DropDown, lvgl.DROPDOWN_DIR_RIGHT)
        --设置 DropDown 的标志图
        lvgl.dropdown_set_symbol(demo_DropDown, lvgl.SYMBOL_RIGHT)
        --添加样式
        lvgl.obj_add_style(demo_DropDown, lvgl.CPICKER_PART_MAIN, demo_ThemeStyle_Bg)
    end

    function demo_GaugeInit()
        --创建一个颜色
        green = lvgl.color_make(0, 255, 0)
        --创建一个 Gauge
        demo_Gauge = lvgl.gauge_create(lvgl.scr_act(), nil)
        --设置 gauge 的表盘指针数
        lvgl.gauge_set_needle_count(demo_Gauge, 1, green)
        --设置表针的值
        lvgl.gauge_set_value(demo_Gauge, 0, 50)
        --设置 Gauge 的大小
        lvgl.obj_set_size(demo_Gauge, 440, 440)
        --设置 Gauge 的位置
        lvgl.obj_align(demo_Gauge, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --设置 Gauge 里的刻度的最小最大值
        lvgl.gauge_set_range(demo_Gauge, 0, 300)
        --设置 Gauge 里的填充的面积，360为不填充
        lvgl.gauge_set_critical_value(demo_Gauge, 100)
        --添加样式
        lvgl.obj_add_style(demo_Gauge, lvgl.GAUGE_PART_MAIN, demo_ThemeStyle_Bg)
    end

    function demo_ImageInit()
        --创建一个 Image
        demo_Image = lvgl.img_create(lvgl.scr_act(), nil)
        --设置 Image 的内容
        lvgl.img_set_src(demo_Image, "/lua/Demo_Img_Big.jpg")
        --设置 Image 的大小(也可以不设置,则显示图片本来的大小)
        lvgl.obj_set_size(demo_Image, 480, 854)
        --设置 Image 的位置
        lvgl.obj_align(demo_Image, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_Image, lvgl.IMG_PART_MAIN, demo_ThemeStyle_Bg)
    end

    function demo_LabelInit()
        --创建一个 Label
        demo_Label = lvgl.label_create(lvgl.scr_act(), nil)
        --设置 Label 的长屏滚动显示
        lvgl.label_set_long_mode(demo_Label, lvgl.LABEL_LONG_SROLL_CIRC)
        --设置 Label 的显示长度(若设置了显示方式，则需要在其后设置才会有效)
        lvgl.obj_set_width(demo_Label, 250)
        --设置 Label 的内容
        lvgl.label_set_text(demo_Label, "To be or not to be, That's a question!")
        --设置 Label 的位置
        lvgl.obj_align(demo_Label, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_Label, lvgl.LABEL_PART_MAIN, demo_ThemeFontStyle)
    end

    function demo_LEDlInit()
        --创建一个 LED1
        demo_LED1 = lvgl.led_create(lvgl.scr_act(), nil)
        lvgl.obj_set_size(demo_LED1, 80, 80)
        --关闭 LED
        lvgl.led_off(demo_LED1)
        --设置 LED 的位置
        lvgl.obj_align(demo_LED1, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, -140)
    --------------------------------------------------------------------------------------
        --创建一个 LED2
        demo_LED2 = lvgl.led_create(lvgl.scr_act(), nil)
        lvgl.obj_set_size(demo_LED2, 80, 80)
        --设置 LED 的亮度
        lvgl.led_set_bright(demo_LED2, 200)
        --设置 LED 的位置
        lvgl.obj_align(demo_LED2, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
    --------------------------------------------------------------------------------------
        --创建一个 LED3
        demo_LED3 = lvgl.led_create(lvgl.scr_act(), nil)
        lvgl.obj_set_size(demo_LED3, 80, 80)
        --打开 LED
        lvgl.led_on(demo_LED3)
        --设置 LED 的位置
        lvgl.obj_align(demo_LED3, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 140)
        --添加样式
        lvgl.obj_add_style(demo_LED1, lvgl.LED_PART_MAIN, demo_LEDStyle_Red)
        lvgl.obj_add_style(demo_LED2, lvgl.LED_PART_MAIN, demo_LEDStyle_Yellow)
        lvgl.obj_add_style(demo_LED3, lvgl.LED_PART_MAIN, demo_LEDStyle_Green)
    end

    function demo_LineMeterInit()
        --创建一个 LineMeter
        demo_LineMeter = lvgl.linemeter_create(lvgl.scr_act(), nil)
        --设置 LineMeter 的大小
        lvgl.obj_set_size(demo_LineMeter, 440, 440)
        --设置 LineMeter 的角度(0-360)和线条数
        lvgl.linemeter_set_scale(demo_LineMeter, 360, 60)
    
        lvgl.linemeter_set_range(demo_LineMeter, 0, 360)
        lvgl.linemeter_set_value(demo_LineMeter, 180)
        --设置 LineMeter 的位置
        lvgl.obj_align(demo_LineMeter, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_LineMeter, lvgl.LINEMETER_PART_MAIN, demo_LineMeterStyle)
    end

    function demo_ListInit()
        --创建一个 List
        demo_List = lvgl.list_create(lvgl.scr_act(), nil)
        --设置 List 的大小
        lvgl.obj_set_height(demo_List, 300)
        --设置 List 的滚动模式
        lvgl.list_set_scrollbar_mode(demo_List, lvgl.SCROLLBAR_MODE_OFF)
        --添加 List 的内容
        --需要添加 lvsym.lua 才可以使用LittleVGL内置的文字图片
        new = lvgl.list_add_btn(demo_List, lvgl.SYMBOL_NEW_LINE, "New")
        open = lvgl.list_add_btn(demo_List, lvgl.SYMBOL_DIRECTORY, "Open")
        delete = lvgl.list_add_btn(demo_List, lvgl.SYMBOL_CLOSE, "Delete")
        edit = lvgl.list_add_btn(demo_List, lvgl.SYMBOL_EDIT, "Edit")
        usb = lvgl.list_add_btn(demo_List, lvgl.SYMBOL_USB, "USB")
        gps = lvgl.list_add_btn(demo_List, lvgl.SYMBOL_GPS, "GPS")
        stop = lvgl.list_add_btn(demo_List, lvgl.SYMBOL_STOP, "Stop")
        video = lvgl.list_add_btn(demo_List, lvgl.SYMBOL_VIDEO, "Video")
        --设置 List 的位置
        lvgl.obj_align(demo_List, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_List, lvgl.LIST_PART_BG, demo_ThemeStyle_Bg)
        lvgl.obj_add_style(new, lvgl.LIST_PART_BG, demo_ThemeStyle_Bg)
        lvgl.obj_add_style(open, lvgl.LIST_PART_BG, demo_ThemeStyle_Bg)
        lvgl.obj_add_style(delete, lvgl.LIST_PART_BG, demo_ThemeStyle_Bg)
        lvgl.obj_add_style(edit, lvgl.LIST_PART_BG, demo_ThemeStyle_Bg)
        lvgl.obj_add_style(usb, lvgl.LIST_PART_BG, demo_ThemeStyle_Bg)
        lvgl.obj_add_style(gps, lvgl.LIST_PART_BG, demo_ThemeStyle_Bg)
        lvgl.obj_add_style(stop, lvgl.LIST_PART_BG, demo_ThemeStyle_Bg)
        lvgl.obj_add_style(video, lvgl.LIST_PART_BG, demo_ThemeStyle_Bg)
        
    end

    function demo_PageInit()
        --创建一个 Page
        demo_Page = lvgl.page_create(lvgl.scr_act(), nil)
        --设置 Page 的大小(显示大小)
        lvgl.obj_set_size(demo_Page, 400, 600)
        --设置 Page 的滚动模式
        lvgl.page_set_scrollbar_mode(demo_Page, lvgl.SCROLLBAR_MODE_OFF)
        --设置 Page 的位置
        lvgl.obj_align(demo_Page, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_Page, lvgl.PAGE_PART_BG, demo_PageStyle)
    
        demo_PageBtn1 = lvgl.btn_create(demo_Page, nil)
        lvgl.obj_set_size(demo_PageBtn1, 100, 100)
        lvgl.obj_align(demo_PageBtn1, demo_Page, lvgl.ALIGN_IN_TOP_MID, 0, 50)
        demo_PageBtn2 = lvgl.btn_create(demo_Page, nil)
        lvgl.obj_set_size(demo_PageBtn2, 100, 100)
        lvgl.obj_align(demo_PageBtn2, demo_Page, lvgl.ALIGN_IN_TOP_MID, 0, 300)
        demo_PageBtn3 = lvgl.btn_create(demo_Page, nil)
        lvgl.obj_set_size(demo_PageBtn3, 100, 100)
        lvgl.obj_align(demo_PageBtn3, demo_Page, lvgl.ALIGN_IN_TOP_MID, 0, 550)
    
        lvgl.obj_add_style(demo_PageBtn1, lvgl.BTN_PART_MAIN, demo_ThemeStyle_IndicAndFont)
        lvgl.obj_add_style(demo_PageBtn2, lvgl.BTN_PART_MAIN, demo_ThemeStyle_IndicAndFont)
        lvgl.obj_add_style(demo_PageBtn3, lvgl.BTN_PART_MAIN, demo_ThemeStyle_IndicAndFont)
    end

    function demo_RollerInit()
        --创建一个 Roller
        demo_Roller = lvgl.roller_create(lvgl.scr_act(), nil)
        --设置 Roller 的显示大小
        lvgl.obj_set_size(demo_Roller, 400, 250)
        --设置 Roller 的内容及滚动方式
        lvgl.roller_set_options(demo_Roller, "东皇钟\n轩辕剑\n盘古斧\n炼妖壶\n昊天塔\n伏羲琴\n神农鼎\n崆峒印\n昆仑镜\n女娲石", lvgl.ROLLER_MODE_INFINITE)
        --设置 Roller 的位置
        lvgl.obj_align(demo_Roller, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_Roller, lvgl.ROLLER_PART_BG, demo_ThemeStyle_Bg)
    end

    function demo_SliderInit()
        --创建一个 Slider
        demo_Slider = lvgl.slider_create(lvgl.scr_act(), nil)
        --设置 Slider 的显示大小
        lvgl.obj_set_size(demo_Slider, 400, 60)
        --设置 Slider 的取值范围
        lvgl.slider_set_range(demo_Slider, 0, 100)
        --设置 Slider 的动画时间
        lvgl.slider_set_anim_time(demo_Slider, 3000)
        --设置 Slider 的值和是否开启动画
        lvgl.slider_set_value(demo_Slider, 100, lvgl.ANIM_ON)
        --设置 Slider 的位置
        lvgl.obj_align(demo_Slider, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_Slider, lvgl.SLIDER_PART_BG, demo_ThemeStyle_Bg)
    end

    function demo_SpinnerInit()
        --创建一个 Spinner
        demo_Spinner = lvgl.spinner_create(lvgl.scr_act(), nil)
        --设置 Spinner 的显示大小
        lvgl.obj_set_size(demo_Spinner, 400, 400)
        --设置 Spinner 的位置
        lvgl.obj_align(demo_Spinner, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_Spinner, lvgl.SPINNER_PART_BG, demo_ThemeStyle_Bg)
    end

    function demo_SwitchInit()
        --创建一个 Switch
        demo_Switch = lvgl.switch_create(lvgl.scr_act(), nil)
        --设置 Switch 的显示大小
        lvgl.obj_set_size(demo_Switch, 150, 60)
        --设置 Switch 的位置
        lvgl.obj_align(demo_Switch, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_Switch, lvgl.SWITCH_PART_BG, demo_ThemeStyle_Bg)
    end

    function demo_TableInit()
        --创建一个 Table
        demo_Table = lvgl.table_create(lvgl.scr_act(), nil)
        --设置 Table 的行数
        lvgl.table_set_row_cnt(demo_Table, 3)
        --设置 Table 的列数
        lvgl.table_set_col_cnt(demo_Table, 2)
    
        --设置 Table 的内容
        lvgl.table_set_cell_value(demo_Table, 0, 0, "阵营")
        lvgl.table_set_cell_value(demo_Table, 1, 0, "博派")
        lvgl.table_set_cell_value(demo_Table, 2, 0, "狂派")
        
        lvgl.table_set_cell_value(demo_Table, 0, 1, "首领")
        lvgl.table_set_cell_value(demo_Table, 1, 1, "擎天柱")
        lvgl.table_set_cell_value(demo_Table, 2, 1, "威震天")
    
        --设置 Table 的显示大小
        lvgl.obj_set_size(demo_Table, 150, 60)
        --设置 Table 的位置
        lvgl.obj_align(demo_Table, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, -50, -50)
        --添加样式
        lvgl.obj_add_style(demo_Table, lvgl.TABLE_PART_BG, demo_ThemeStyle_Bg)
    end

    function demo_TabViewInit()
        --创建一个 TabView
        demo_TabView = lvgl.tabview_create(lvgl.scr_act(), nil)
        --设置 TabView 的显示大小
        lvgl.obj_set_size(demo_TabView, 440, 600)
        --设置 TabView 的位置
        lvgl.obj_align(demo_TabView, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --添加样式
        lvgl.obj_add_style(demo_TabView, lvgl.TABVIEW_PART_BG, demo_ThemeStyle_Bg)
        lvgl.obj_add_style(demo_TabView, lvgl.TABVIEW_PART_TAB_BG, demo_ThemeStyle_Bg)
    
        --添加页面
        demo_Tab1 = lvgl.tabview_add_tab(demo_TabView, "电影")
        demo_Tab2 = lvgl.tabview_add_tab(demo_TabView, "电视剧")
    end

    function demo_WindowInit()
        --创建一个 Switch
        demo_Window = lvgl.win_create(lvgl.scr_act(), nil)
        --设置 Switch 的显示大小
        lvgl.obj_set_size(demo_Window, 440, 600)
        --设置 Switch 的位置
        lvgl.obj_align(demo_Window, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
        --设置 Window 的标题
        lvgl.win_set_title(demo_Window, "热点新闻")
        --添加样式
        lvgl.obj_add_style(demo_Window, lvgl.WIN_PART_BG, demo_ThemeStyle_Bg)
    end

    DEMO_BASE_CONT = nil

    local data = {type = lvgl.INDEV_TYPE_POINTER}
    local function input()
	    pmd.sleep(100)
	    local ret,ispress,px,py = tp.get()
	    if ret then
		    if lastispress == ispress and lastpx == px and lastpy == py then
		    	return data
		    end
		    lastispress = ispress
		    lastpx = px
		    lastpy = py
		    if ispress then
		    	tpstate = lvgl.INDEV_STATE_PR
		    else
		    	tpstate = lvgl.INDEV_STATE_REL
		    end
	    else
		    return data
	    end

	    local topoint = {x = px,y = py}
	    data.state = tpstate
	    data.point = topoint

	    return data
    end

    function demoInit()
	    --为Demo创建一个共同的父级容器
	    DEMO_BASE_CONT = lvgl.cont_create(lvgl.scr_act(), nil)
	    --设置父级容器的大小
	    lvgl.obj_set_size(DEMO_BASE_CONT, 480, 854)
	    --设置父级容器的位置(对齐方式)
	    lvgl.obj_align(DEMO_BASE_CONT, nil, lvgl.ALIGN_CENTER, 0, 0)
	    --为父级容器添加样式
	    lvgl.obj_add_style(DEMO_BASE_CONT, lvgl.CONT_PART_MAIN, demo_BaseContStyle)
        sys.taskInit(function ()
	       while true do
              demo_ArcInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_Arc)
              demo_BarInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_Bar)
              demo_BtnInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_Btn)
              demo_CalendarInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_Calendar)
              demo_ChartInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_Chart)
              demo_CheckBoxInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_CheckBox)
              demo_CPickerInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_CPicker)
              demo_ContInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_Cont)
              demo_DropDownInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_DropDown)
              demo_GaugeInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_Gauge)
              demo_LabelInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_Label)
              demo_LEDlInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_LED1)
              lvgl.obj_del(demo_LED2)
              lvgl.obj_del(demo_LED3)
              demo_ListInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_List)
              demo_LineMeterInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_LineMeter)
              demo_PageInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_Page)
              demo_RollerInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_Roller)
              demo_SliderInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_Slider)
              demo_SwitchInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_Switch)
              demo_TableInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_Table)
              demo_TabViewInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_TabView)
              demo_WindowInit()
              sys.wait(waitTime2)
		      lvgl.obj_del(demo_Window)
           end
        end)
    end

    local function init()
    	lvgl.init(demoInit, input)
    	pmd.ldoset(8,pmd.LDO_VIBR)
    end

    init()

elseif LuaTaskTestConfig.dispTest.lvglTestV7_little then
    sys.taskInit(function ()
        sys.wait(1000)
        while true do   
            -- 创建曲线
            arc = lvgl.arc_create(lvgl.scr_act(), nil)
            -- 设置尺寸
            lvgl.obj_set_size(arc, 150, 150)
            -- 设置位置居中
            lvgl.obj_align(arc, nil, lvgl.ALIGN_CENTER, 0, 0)
            -- 绘制弧度
            lvgl.arc_set_end_angle(arc, 200)
            sys.wait(3000)
		    lvgl.obj_del(arc)
   
            -- 按键回调函数
            event_handler = function(obj, event)
                if event == lvgl.EVENT_CLICKED then
                    print("Clicked\n")
                elseif event == lvgl.EVENT_VALUE_CHANGED then
                    print("Toggled\n")
                end
            end
            -- 按键1
            btn1 = lvgl.btn_create(lvgl.scr_act(), nil)
            lvgl.obj_set_event_cb(btn1, event_handler)
            lvgl.obj_align(btn1, nil, lvgl.ALIGN_CENTER, 0, -40)
            -- 按键1 的文字
            label = lvgl.label_create(btn1, nil)
            lvgl.label_set_text(label, "Button")
            -- 按键2
            btn2 = lvgl.btn_create(lvgl.scr_act(), nil)
            lvgl.obj_set_event_cb(btn2, event_handler)
            lvgl.obj_align(btn2, nil, lvgl.ALIGN_CENTER, 0, 40)
            lvgl.btn_set_checkable(btn2, true)
            lvgl.btn_toggle(btn2)
            lvgl.btn_set_fit2(btn2, lvgl.FIT_NONE, lvgl.FIT_TIGHT)
            -- 按键2 的文字
            label = lvgl.label_create(btn2, nil)
            lvgl.label_set_text(label, "Toggled")
            sys.wait(3000)
		    lvgl.obj_del(btn1)
            lvgl.obj_del(btn2)

            -- 创建进度条
            bar = lvgl.bar_create(lvgl.scr_act(), nil)
            -- 设置尺寸
            lvgl.obj_set_size(bar, 100, 10);
            -- 设置位置居中
            lvgl.obj_align(bar, NULL, lvgl.ALIGN_CENTER, 0, 0)
            -- 设置加载完成时间
            lvgl.bar_set_anim_time(bar, 2000)
            -- 设置加载到的值
            lvgl.bar_set_value(bar, 100, lvgl.ANIM_ON)
            sys.wait(3000)
		    lvgl.obj_del(bar)



            -- 高亮显示的日期
            highlightDate = lvgl.calendar_date_t() 
            -- 日历点击的回调函数
            -- 将点击日期设置高亮
            function event_handler(obj, event)
                if event == lvgl.EVENT_VALUE_CHANGED then
                    date = lvgl.calendar_get_pressed_date(obj)
                    if date then
                        print(string.format("Clicked date: %02d.%02d.%d\n", date.day, date.month, date.year))
                        highlightDate.year = date.year
                        highlightDate.month = date.month
                        highlightDate.day = date.day
                        lvgl.calendar_set_highlighted_dates(obj, highlightDate, 1)
                    end
                end
            end
            -- 创建日历
            calendar = lvgl.calendar_create(lvgl.scr_act(), nil)
            lvgl.obj_set_size(calendar, 235, 235)
            lvgl.obj_align(calendar, nil, lvgl.ALIGN_CENTER, 0, 0)
            lvgl.obj_set_event_cb(calendar, event_handler)
            
            -- 设置今天日期
            today = lvgl.calendar_date_t()
            today.year = 2018
            today.month = 10
            today.day = 23
            
            lvgl.calendar_set_today_date(calendar, today)
            lvgl.calendar_set_showed_date(calendar, today)
            sys.wait(3000)
		    lvgl.obj_del(calendar)

            -- 复选框回调函数
            function event_handler(obj, event)
                if event == lvgl.EVENT_VALUE_CHANGED then
                    print("State", lvgl.checkbox_is_checked(obj))
                end
            end
            -- 创建复选框
            cb = lvgl.checkbox_create(lvgl.scr_act(), nil)
            -- 设置标签
            lvgl.checkbox_set_text(cb, "hello")
            -- 设置居中位置
            lvgl.obj_align(cb, nil, lvgl.ALIGN_CENTER, 0, 0)
            -- 设置回调函数
            lvgl.obj_set_event_cb(cb, event_handler)
            sys.wait(3000)
		    lvgl.obj_del(cb)

            -- 创建容器
            cont = lvgl.cont_create(lvgl.scr_act(), nil)
            lvgl.obj_set_auto_realign(cont, true)                   
            lvgl.obj_align(cont, nil, lvgl.ALIGN_CENTER, 0, 0) 
            lvgl.cont_set_fit(cont, lvgl.FIT_TIGHT)
            lvgl.cont_set_layout(cont, lvgl.LAYOUT_COLUMN_MID) 
            -- 添加标签
            label = lvgl.label_create(cont, nil)
            lvgl.label_set_text(label, "Short text")
            sys.wait(3000)
		    lvgl.obj_del(label)
            lvgl.obj_del(cont)


            -- 创建图表
            chart = lvgl.chart_create(lvgl.scr_act(), nil)
            lvgl.obj_set_size(chart, 200, 150)
            lvgl.obj_align(chart, nil, lvgl.ALIGN_CENTER, 0, 0)
            
            -- 设置 Chart 的显示模式 (折线图)
            lvgl.chart_set_type(chart, lvgl.CHART_TYPE_LINE)  
            
            ser1 = lvgl.chart_add_series(chart, lvgl.color_hex(0xFF0000))
            ser2 = lvgl.chart_add_series(chart, lvgl.color_hex(0x008000))
            
            -- 添加点
            for i=0, 15 do
                lvgl.chart_set_next(chart, ser1, math.random(10, 90))
            end
            
            for i=15, 0, -1 do
                lvgl.chart_set_next(chart, ser2, math.random(10, 90))
            end
            -- 刷新图表
            lvgl.chart_refresh(chart)
            sys.wait(3000)
		    lvgl.obj_del(chart)

          
            -- 回调函数
             event_handler = function(obj, event)
                 if (event == lvgl.EVENT_VALUE_CHANGED) then
                     print("Option:", lvgl.dropdown_get_symbol(obj))
                 end
             end
             -- 创建下拉框
             dd = lvgl.dropdown_create(lvgl.scr_act(), nil)
             lvgl.dropdown_set_options(dd, [[Apple
             Banana
             Orange
             Cherry
             Grape
             Raspberry
             Melon
             Orange
             Lemon
             Nuts]])
             -- 设置对齐
             lvgl.obj_align(dd, nil, lvgl.ALIGN_IN_TOP_MID, 0, 20)
             lvgl.obj_set_event_cb(dd, event_handler)
             sys.wait(5000)
             lvgl.obj_del(dd)

             function keyCb(obj, e)
                -- 默认处理事件
                lvgl.keyboard_def_event_cb(keyBoard, e)
                if(e == lvgl.EVENT_CANCEL)then
                    lvgl.keyboard_set_textarea(keyBoard, nil)
                    --删除 KeyBoard
                    lvgl.obj_del(keyBoard)
                    keyBoard = nil
                end
            end
            function textAreaCb(obj, e)
                if (e == lvgl.EVENT_CLICKED) and not keyBoard then
                    --创建一个 KeyBoard
                    keyBoard = lvgl.keyboard_create(lvgl.scr_act(), nil)
                    --设置 KeyBoard 的光标是否显示
                    lvgl.keyboard_set_cursor_manage(keyBoard, true)
                    --为 KeyBoard 设置一个文本区域
                    lvgl.keyboard_set_textarea(keyBoard, textArea)
                    lvgl.obj_set_event_cb(keyBoard, keyCb)
                end
            end
            textArea = lvgl.textarea_create(lvgl.scr_act(), nil)
            lvgl.obj_set_size(textArea, 100, 25)
            lvgl.textarea_set_text(textArea, "please input:")
            lvgl.obj_align(textArea, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, -45)
            lvgl.obj_set_event_cb(textArea, textAreaCb)
            sys.wait(3000)
            lvgl.obj_del(textArea)


            label = lvgl.label_create(lvgl.scr_act(), nil)
            lvgl.label_set_recolor(label, true)                     
            lvgl.label_set_text(label, "#0000ff Re-color# #ff00ff words# #ff0000 of\n# align the lines to\n the center and wrap\n long text automatically.")
            lvgl.obj_set_width(label, 100)  
            lvgl.label_set_align(label, lvgl.LABEL_ALIGN_CENTER)
            lvgl.obj_align(label, nil, lvgl.ALIGN_CENTER, 0, -40)
            sys.wait(3000)
            lvgl.obj_del(label)
  
            spinner = lvgl.spinner_create(lvgl.scr_act(), nil)
            lvgl.obj_set_size(spinner, 100, 100)
            lvgl.obj_align(spinner, nil, lvgl.ALIGN_CENTER, 0, 0) 
            sys.wait(3000)
            lvgl.obj_del(spinner)

            function event_handler(obj, event)
                if event == lvgl.EVENT_VALUE_CHANGED then
                    print("State", lvgl.switch_get_state(obj))
                end
            end
            sw1 = lvgl.switch_create(lvgl.scr_act(), nil)
            lvgl.obj_align(sw1, nil, lvgl.ALIGN_CENTER, 0, -50)
            lvgl.obj_set_event_cb(sw1, event_handler)
            sw2 = lvgl.switch_create(lvgl.scr_act(), sw1)
            lvgl.switch_on(sw2, lvgl.ANIM_ON)
            lvgl.obj_align(sw2, nil, lvgl.ALIGN_CENTER, 0, 50)
            sys.wait(3000)
            lvgl.obj_del(sw1)
            lvgl.obj_del(sw2)


            --创建一个颜色
            green = lvgl.color_make(0, 255, 0)
            --创建一个 Gauge
            demo_Gauge = lvgl.gauge_create(lvgl.scr_act(), nil)
            --设置 gauge 的表盘指针数
            lvgl.gauge_set_needle_count(demo_Gauge, 1, green)
            --设置表针的值
	        lvgl.gauge_set_value(demo_Gauge, 0, 50)
            --设置 Gauge 的大小
            lvgl.obj_set_size(demo_Gauge, 100, 100)
            --设置 Gauge 的位置
            lvgl.obj_align(demo_Gauge, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, 0, 0)
            --设置 Gauge 里的刻度的最小最大值
            lvgl.gauge_set_range(demo_Gauge, 0, 300)
            --设置 Gauge 里的填充的面积，360为不填充
            lvgl.gauge_set_critical_value(demo_Gauge, 100)
        
            --添加样式
            lvgl.obj_add_style(demo_Gauge, lvgl.GAUGE_PART_MAIN, demo_ThemeStyle_Bg)
            sys.wait(3000)
            lvgl.obj_del(demo_Gauge)



            --创建一个 Table
            demo_Table = lvgl.table_create(lvgl.scr_act(), nil)
            --设置 Table 的行数
            lvgl.table_set_row_cnt(demo_Table, 3)
            --设置 Table 的列数
            lvgl.table_set_col_cnt(demo_Table, 2)
        
            --设置 Table 的内容
            lvgl.table_set_cell_value(demo_Table, 0, 0, "阵营")
            lvgl.table_set_cell_value(demo_Table, 1, 0, "博派")
            lvgl.table_set_cell_value(demo_Table, 2, 0, "狂派")
            
            lvgl.table_set_cell_value(demo_Table, 0, 1, "首领")
            lvgl.table_set_cell_value(demo_Table, 1, 1, "擎天柱")
            lvgl.table_set_cell_value(demo_Table, 2, 1, "威震天")
        
            --设置 Table 的显示大小
            lvgl.obj_set_size(demo_Table, 30, 20)
            --设置 Table 的位置
            lvgl.obj_align(demo_Table, DEMO_BASE_CONT, lvgl.ALIGN_CENTER, -50, -50)
            --添加样式
            lvgl.obj_add_style(demo_Table, lvgl.TABLE_PART_BG, demo_ThemeStyle_Bg)
            sys.wait(3000)
            lvgl.obj_del(demo_Table)

    
        end
    
    end)
    
    sys.init(0, 0)
    sys.run()
    
elseif LuaTaskTestConfig.dispTest.dispTest then 
    sys.taskInit(function()

        local count = 1

        sys.wait(5000)

        while true do

            if LuaTaskTestConfig.dispTest.logoTest then
                log.info("DispTest.LogoTest", "第" .. count .. "次")
                -- 显示logo
                -- 清空LCD显示缓冲区
                disp.clear()
                -- 从坐标16,0位置开始显示"欢迎使用Luat"
                log.info("DispTest.PutText", "LuatTest" .. count)
                disp.puttext(common.utf8ToGb2312("LuatTest" .. count),
                             getxpos(common.utf8ToGb2312("LuatTest" .. count)),
                             0)
                -- 显示logo图片
                log.info("DispTest.PutImage", "Logo_color")
                disp.putimage("/lua/logo_color.png", 1, 33)
                -- 刷新LCD显示缓冲区到LCD屏幕上
                disp.update()
                sys.wait(waitTime2)
            end

            if LuaTaskTestConfig.dispTest.scanTest then
                log.info("DispTest.ScanTest", "第" .. count .. "次")
                pm.wake("DispTest.ScanTest")
                local ret = 0
                log.info("DispTest.ScanTest", "开始扫描")
                -- 设置扫码回调函数，默认10秒超时
                scanCode.request(scanCodeCb)
                -- 打开摄像头
                ret = disp.cameraopen_ext(gc0310_sdr)
                -- 打开摄像头预览   
                -- log.info("DispTest.scan cameraopen_ext ret ", ret)
                -- disp.camerapreviewzoom(-2)

                ret = disp.camerapreview(0, 0, 0, 0, WIDTH, HEIGHT)

                -- log.info("DispTest.scan camerapreview ret ", ret)
                sys.wait(10000)
            end

            if LuaTaskTestConfig.dispTest.photoTest then
                log.info("DispTest.PhotoTest", "第" .. count .. "次")
                -- 拍照并显示
                pm.wake("DispTest.PhotoTest")
                -- 打开摄像头
                disp.cameraopen(1, 0, 0, 1)
                -- 打开摄像头预览
                disp.camerapreview(0, 0, 0, 0, WIDTH, HEIGHT)
                -- 设置照片的宽和高像素并且开始拍照
                disp.cameracapture(WIDTH, HEIGHT)
                -- 设置照片保存路径
                disp.camerasavephoto("/testCamera.jpg")
                log.info("DispTest.PhotoSize", io.fileSize("/testCamera.jpg"))
                -- 关闭摄像头预览
                disp.camerapreviewclose()
                -- 关闭摄像头
                disp.cameraclose()
                -- 允许系统休眠
                pm.sleep("DispTest.PhotoTest")
                -- 显示拍照图片   
                if WIDTH ~= 0 and HEIGHT ~= 0 then
                    disp.clear()
                    disp.putimage("/testCamera.jpg", 0, 0)
                    disp.puttext(common.utf8ToGb2312("照片尺寸: " ..
                                                         io.fileSize(
                                                             "/testCamera.jpg")),
                                 0, 5)
                    disp.update()
                end
                sys.wait(waitTime2)
            end

            if LuaTaskTestConfig.dispTest.photoSendTest then
                log.info("DispTest.PhotoSendTest", "第" .. count .. "次")
                -- 拍照并通过uart1发送出去
                pm.wake("DispTest.PhotoSendTest")
                -- 打开摄像头
                disp.cameraopen(1, 0, 0, 1)
                -- 打开摄像头预览
                disp.camerapreview(0, 0, 0, 0, WIDTH, HEIGHT)
                -- 设置照片的宽和高像素并且开始拍照
                disp.cameracapture(WIDTH, HEIGHT)
                -- 设置照片保存路径
                disp.camerasavephoto("/testCamera.jpg")
                log.info("DispTest.PhotoSize", io.fileSize("/testCamera.jpg"))
                -- 关闭摄像头预览
                disp.camerapreviewclose()
                -- 关闭摄像头
                disp.cameraclose()
                -- 允许系统休眠
                pm.sleep("DispTest.PhotoSendTest")

                sendFile(1)
                if WIDTH ~= 0 and HEIGHT ~= 0 then
                    disp.clear()
                    disp.putimage("/testCamera.jpg", 0, 0)
                    disp.puttext(common.utf8ToGb2312("照片尺寸: " ..
                                                         io.fileSize(
                                                             "/testCamera.jpg")),
                                 0, 5)
                    disp.update()
                end
                sys.wait(waitTime2)
            end

            if LuaTaskTestConfig.dispTest.qrcodeTest then
                log.info("DispTest.QrCodeTest", "第" .. count .. "次")
                -- 显示二维码
                disp.clear()
                local displayWidth = 100
                disp.puttext(common.utf8ToGb2312("二维码生成测试"),
                             getxpos(
                                 common.utf8ToGb2312("二维码生成测试")),
                             10)
                disp.putqrcode(data, qrCodeWidth, displayWidth,
                               (WIDTH1 - displayWidth) / 2,
                               (HEIGHT1 - displayWidth) / 2)
                disp.update()
                sys.wait(waitTime2)
            end

            if LuaTaskTestConfig.dispTest.uiWinTest then
                log.info("DispTest.UIWinTest", "第" .. count .. "次")
                -- 1秒后，打开提示框窗口，提示"3秒后进入待机界面"
                -- 提示框窗口关闭后，自动进入待机界面
                sys.timerStart(openprompt, 1000, common.utf8ToGb2312("3秒后"),
                               common.utf8ToGb2312("进入待机界面"), nil,
                               openidle)
                sys.wait(waitTime2)
            end

            count = count + 1

        end
    end)
end
