-- LbsLocTest
-- Author:LuatTest
-- CreateDate:20201013
-- UpdateDate:20210727

module(..., package.seeall)

local gpsModType = LuaTaskTestConfig.gpsModType
local serverAddr = "http://114.55.242.59:2900"

local postCellLocInfoAddress = serverAddr .. "/postCellLocInfo"
local postWiFiLocInfoAddress = serverAddr .. "/postWiFiLocInfo"
local postGPSLocInfoAddress = serverAddr .. "/postGPSLocInfo"

local cellLattmp, cellLngtmp = 0, 0
local wifiLattmp, wifiLngtmp = 0, 0
local gpsLattmp, gpsLngtmp = "", ""

local loopTime = 30000

local cellSendToServer, wifiSendToServer, GPSSendToServer = false, false, false

local function getCellLocCb(result, lat, lng, addr)
    log.info("CellLocTest.getCellLocCb.result", result)
    log.info("CellLocTest.getCellLocCb.lat", lat)
    log.info("CellLocTest.getCellLocCb.lng", lng)
    if result == 0 then
        log.info("CellLocTest.getCellLocCb", "SUCCESS")
        if cellSendToServer == true then
            if lat == cellLattmp and lng == cellLngtmp then
                log.info("CellLocTest.getCellLocCb", "基站定位信息未发生改变，本次定位结果不上传服务器")
            else
                cellLattmp = lat
                cellLngtmp = lng
                local cellLocInfo = {
                    lat = lat,
                    lng = lng,
                    timestamp = os.time()
                }
                http.request(
                    "POST",
                    postCellLocInfoAddress,
                    nil,
                    {
                        ["Content-Type"] = "application/json",
                    },
                    json.encode(cellLocInfo),
                    nil,
                    function (result, prompt, head, body)
                        if result then
                            log.info("CellLocTest.postLbsLocInfo.result", "SUCCESS")
                        else
                            log.info("CellLocTest.postLbsLocInfo.result", "FAIL")
                        end
                        log.info("CellLocTest.postLbsLocInfo.prompt", "Http状态码:", prompt)
                        if result and head then
                            log.info("CellLocTest.postLbsLocInfo.Head", "遍历响应头")
                            for k, v in pairs(head) do
                                log.info("CellLocTest.postLbsLocInfo.Head", k .. " : " .. v)
                            end
                        end
                        if result and body then
                            log.info("CellLocTest.postLbsLocInfo.Body", "body=" .. body)
                            log.info("CellLocTest.postLbsLocInfo.Body", "bodyLen=" .. body:len())
                        end
                    end
                )
            end
        end
    else
        log.info("CellLocTest.getLocCb", "FAIL")
    end
end

local function getWiFiLocCb(result, lat, lng, addr)
    log.info("WifiLocTest.getWiFiLocCb.result", result)
    log.info("WifiLocTest.getWiFiLocCb.lat", lat)
    log.info("WifiLocTest.getWiFiLocCb.lng", lng)
    if result == 0 then
        log.info("WifiLocTest.getWiFiLocCb", "SUCCESS")
        if wifiSendToServer == true then
            if lat == wifiLattmp and lng == wifiLngtmp then
                log.info("WifiLocTest.getWiFiLocCb", "WiFi定位信息未发生改变，本次定位结果不上传服务器")
            else
                wifiLattmp = lat
                wifiLngtmp = lng
                local wifiLocInfo = {
                    lat = lat,
                    lng = lng,
                    timestamp = os.time()
                }
                http.request(
                    "POST",
                    postWiFiLocInfoAddress,
                    nil,
                    {
                        ["Content-Type"] = "application/json",
                    },
                    json.encode(wifiLocInfo),
                    nil,
                    function (result, prompt, head, body)
                        if result then
                            log.info("WifiLocTest.postLbsLocInfo.result", "SUCCESS")
                        else
                            log.info("WifiLocTest.postLbsLocInfo.result", "FAIL")
                        end
                        log.info("WifiLocTest.postLbsLocInfo.prompt", "Http状态码:", prompt)
                        if result and head then
                            log.info("WifiLocTest.postLbsLocInfo.Head", "遍历响应头")
                            for k, v in pairs(head) do
                                log.info("WifiLocTest.postLbsLocInfo.Head", k .. " : " .. v)
                            end
                        end
                        if result and body then
                            log.info("WifiLocTest.postLbsLocInfo.Body", "body=" .. body)
                            log.info("WifiLocTest.postLbsLocInfo.Body", "bodyLen=" .. body:len())
                        end
                    end
                )
            end
        end
    else
        log.info("WifiLocTest.getWiFiLocCb", "FAIL")
    end
end

local function wifiLocTest()
    wifiScan.request(
        function(result, cnt, apInfo)
            if result then
                log.info("WifiLocTest.ScanCb", "SUCCESS")
                for k, v in pairs(apInfo) do
                    log.info("WifiLocTest.WifiInfo", k, v)
                end
                log.info("WiFiLocTest", "开始WiFi定位")
                lbsLoc.request(getWiFiLocCb, false, false, false, false, false, false, apInfo)
            else
                log.info("WifiLocTest.ScanCb", "FAIL")
            end
        end
    )
end

local function sendGPSInfoToServer(lat, lng)
    local gpsLocInfo = {
        lat = lat,
        lng = lng,
        timestamp = os.time()
    }
    http.request(
        "POST",
        postGPSLocInfoAddress,
        nil,
        {
            ["Content-Type"] = "application/json",
        },
        json.encode(gpsLocInfo),
        nil,
        function (result, prompt, head, body)
            if result then
                log.info("GPSLocTest.sendGPSInfoToServer.result", "SUCCESS")
            else
                log.info("GPSLocTest.sendGPSInfoToServer.result", "FAIL")
            end
            log.info("GPSLocTest.sendGPSInfoToServer.prompt", "Http状态码:", prompt)
            if result and head then
                log.info("GPSLocTest.sendGPSInfoToServer.Head", "遍历响应头")
                for k, v in pairs(head) do
                    log.info("GPSLocTest.sendGPSInfoToServer.Head", k .. " : " .. v)
                end
            end
            if result and body then
                log.info("GPSLocTest.sendGPSInfoToServer.Body", "body=" .. body)
                log.info("GPSLocTest.sendGPSInfoToServer.Body", "bodyLen=" .. body:len())
            end
        end
    )
end

local function printGpsInfo()
    local tag = "GPSLocTest"
    local selectedGPS
    if gpsModType == "GK" then
        selectedGPS = gps
    elseif gpsModType == "ZKW" then
        selectedGPS = gpsZkw
    elseif gpsModType == "HXXT" then
        selectedGPS = gpsHxxt
    else
        log.error(tag .. ".modType", "GPS模块型号错误FAIL")
        return
    end
    if selectedGPS.isOpen() and selectedGPS.isFix() then
        local tLocation = selectedGPS.getLocation()
        local lat = tLocation.lat
        local lng = tLocation.lng
        log.info(tag .. ".LocInfo", lat, lng)
        -- local UTCTime = selectedGPS.getUtcTime()
        -- log.info("GPSLocTest.UTCTime", string.format("%d-%d-%d %d:%d:%d", UTCTime.year, UTCTime.month, UTCTime.day, UTCTime.hour, UTCTime.min, UTCTime.sec))
        
        if GPSSendToServer == true then
            if lat == gpsLattmp and lng == gpsLngtmp then
                log.info("GPSLocTest", "GPS定位信息未发生改变，本次定位结果不上传服务器")
            else
                gpsLattmp = lat
                gpsLngtmp = lng
                sendGPSInfoToServer(lat, lng)
            end
        end
    end
end

if LuaTaskTestConfig.lbsLocTest.cellLocTest then
    sys.timerLoopStart(
        function()
            log.info("CellLocTest", "开始基站定位")
            lbsLoc.request(getCellLocCb)
        end,
        loopTime
    )
end

if LuaTaskTestConfig.lbsLocTest.wifiLocTest then
    sys.timerLoopStart(wifiLocTest, loopTime)
end

local function nmeaCb(nmeaData)
    if LuaTaskTestConfig.lbsLocTest.rtkTest == true then
        rtk.write(nmeaData)
    end
    -- log.info("GPSTest.nmeaCb", nmeaData)
end

if LuaTaskTestConfig.lbsLocTest.gpsLocTest then
    sys.taskInit(
        function ()
            local tag = "GPSLocTest"
            -- sys.waitUntil("IP_READY_IND")
            sys.wait(8000)

            if LuaTaskTestConfig.lbsLocTest.rtkTest == true then
                rtos.on(
                    rtos.MSG_RTK_INFO, 
                    function (msg)
                        log.info("rtk",msg.id,msg.status,msg.data)
                    end
                )

                local para =
                {
                    appKey = "xyuwwhggzueyiqpgba",
                    appSecret = "",
                    solMode = rtk.SOLMODE_RTK,
                    solSec = rtk.SEC_5S,
                    reqSec = rtk.SEC_5S
                }
                rtk.open(para)
            end

            log.info(tag .. ".open", "打开GPS")
            
            if gpsModType == "GK" then
                require "gps"
                require "agps"

                pmd.ldoset(15, pmd.LDO_VIBR)
                gps.setNmeaMode(2, nmeaCb)
                gps.setUart(3, 115200, 8, uart.PAR_NONE, uart.STOP_1)
                gps.open(gps.DEFAULT, {tag = "GPSLocTest"})
            elseif gpsModType == "ZKW" then
                require "gpsZkw"
                require "agpsZkw"

                gpsZkw.setUart(3, 9600, 8, uart.PAR_NONE, uart.STOP_1)
                gpsZkw.open(gpsZkw.DEFAULT, {tag = "GPSLocTest"})
                gpsZkw.setNmeaMode(2, nmeaCb)
            elseif gpsModType == "HXXT" then
                require "gpsHxxt"
                require "agpsHxxt"

                gpsHxxt.setUart(2, 115200, 8, uart.PAR_NONE, uart.STOP_1)
                gpsHxxt.open(gpsHxxt.DEFAULT, {tag = "GPSLocTest"})
                gpsHxxt.setNmeaMode(2, nmeaCb)
            else
                log.error(tag .. ".modType", "GPS模块型号错误FAIL")
                return
            end

            sys.timerLoopStart(printGpsInfo, 2000)
        end
    )
end