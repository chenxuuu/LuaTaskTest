#### 介绍
合宙非自动化测试脚本，是对LuaTaskTest自动测试脚本和demo的补充

#### 脚本checklist
 
 1) /coreTest
 coreTest脚本，测试基本数传功能（如：socket，ssl，mqtt）、uart和fota功能。

 2) /keep-alive
 通过socketcore.sock_setopt设置Keep-alive功能，对保活功能进行测试

 3) /485
 测试485功能

 4) /bug4890
 此用例是为了验证bug4890。此脚本中连的是一个特殊的TCP服务器，连上这个服务器后，会马上被踢掉。这个是个异常测试流程，验证在不停连接和踢掉的过程中是否会死机。

 5) /8910_V2.1.1 问题验证
 8910 V2.1.1版本虽然废止了，但是版本release notes还是要在后续版本进行验证的。此文件夹下的脚本适用于V3102及以后版本。包括：
 /Task_0 加密算法逻辑
 /Task_0 验证MIPI LCD功能
 /Task_1782 支持sm3算法
 /Task_1811 定时检测脉冲接口

 6) /8910_V3036问题验证
 /Bug_4868(增加语音播放的暂停播放和恢复播放功能)
 /Bug_5000(解决获取蓝牙MAC地址接口需要打开蓝牙的问题)
 /Bug_5015(修改关闭蓝牙没有消息上报的问题)
 /Task_75(添加lua硬流控功能)
 /Task_77(增加gpio模拟io实现多路pwm功能 接口：pio.pin.pwm)
 /Task_80(解除i2c接口波特率限制，i2c_setup接口加入可选参数isbaud实现自定义波特率)
 /Task_1729(BLE蓝牙添加白名单功能)

 7) /8910_V3102问题验证
 /Bug_5168(验证 i2c 波特率设置无效问题）
 /Bug_5183(SIM卡热插拔无+cpin sim removed上报)
 /Task_1811(提供定时检测脉冲的接口)

 8) /8910_V3103问题验证
 /新增U盘功能
 
 + 参考合宙官方教程烧录脚本进行测试