--- 模块功能：参数存储功能测试.
-- @author openLuat
-- @module nvm.testNvm
-- @license MIT
-- @copyright openLuat
-- @release 2018.03.27

module(...,package.seeall)

require"config"
require"nvm"

nvm.init("config.lua")

sys.timerLoopStart(
function()
    log.info("nvm.numPara",nvm.get("numPara"))
    nvm.set("numPara",nvm.get("numPara")+1)
end,
1000
)
