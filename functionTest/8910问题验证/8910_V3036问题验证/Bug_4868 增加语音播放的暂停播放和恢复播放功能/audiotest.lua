
module(...,package.seeall)
--require"record"
require"audio"
require"common"


--播放音频文件测试接口，每次打开一行代码进行测试
local function testPlayFile()
    --单次播放来电铃声，默认音量等级
    --audio.play(CALL,"FILE","/lua/call.mp3")
    --单次播放来电铃声，音量等级7
    --audio.play(CALL,"FILE","/lua/call.mp3",audiocore.VOL7)
    --单次播放来电铃声，音量等级7，播放结束或者出错调用testcb回调函数
    --audio.play(CALL,"FILE","/lua/call.mp3",audiocore.VOL7,testCb)
    --循环播放来电铃声，音量等级7，没有循环间隔(一次播放结束后，立即播放下一次)
    audio.play(CALL,"FILE","/lua/call.mp3",1,nil,true)
    --循环播放来电铃声，音量等级7，循环间隔为2000毫秒
    --audio.play(CALL,"FILE","/lua/call.mp3",audiocore.VOL7,nil,true,2000)
     sys.wait(5000)
     log.info("===========","testAudio.pause")
     audiocore.pause()
     sys.wait(5000)
     log.info("===========","testAudio.resume")
     audiocore.resume()
     sys.wait(5000)

end

sys.taskInit(testPlayFile)