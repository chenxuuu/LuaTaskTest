module(..., package.seeall)

require "wifiScan"

local function init()
    log.info("bt", "init")
    rtos.on(rtos.MSG_BLUETOOTH, function(msg)
        if msg.event == btcore.MSG_OPEN_CNF then
            sys.publish("BT_OPEN", msg.result) --蓝牙打开成功
        elseif msg.event == btcore.MSG_BLE_CONNECT_IND then
            sys.publish("BT_CONNECT_IND", {["handle"] = msg.handle, ["result"] = msg.result}) --蓝牙连接成功
		elseif msg.event == btcore.MSG_BLE_DISCONNECT_IND then
            log.info("bt", "ble disconnect") --蓝牙断开连接
        elseif msg.event == btcore.MSG_BLE_DATA_IND then
            sys.publish("BT_DATA_IND", {["result"] = msg.result})--接收到的数据内容
        end
    end)
end

local function unInit()
    btcore.close()
end

local function poweron()
    log.info("bt", "poweron")
    btcore.open(0) --打开蓝牙从模式
    _, result = sys.waitUntil("BT_OPEN", 5000) --等待蓝牙打开成功
end

local function advertising()
    log.info("bt", "advertising")
    btcore.setname("Luat_Air724UGXXTest")-- 设置广播名称
    btcore.setadvdata(string.fromHex("02010604ff000203"))-- 设置广播数据 根据蓝牙广播包协议
    btcore.setscanrspdata(string.fromHex("04ff000203"))-- 设置广播数据 根据蓝牙广播包协议

    --将有主模式的蓝牙地址添加进入白名单中
    btcore.addwhitelist("DD:DD:11:22:33:44",0)

    btcore.setadvparam(0x80,0xa0,0,0,0x07,2) --广播参数设置 (最小广播间隔,最大广播间隔,广播类型,广播本地地址类型,广播channel map,广播过滤策略)
    
    btcore.advertising(1)-- 打开广播
end

local function data_trans()
    
    advertising()
    _, bt_connect = sys.waitUntil("BT_CONNECT_IND") --等待连接成功
    if bt_connect.result ~= 0 then
        return false    
    end
    --链接成功
    log.info("bt.connect_handle",bt_connect.handle) --连接句柄
    sys.wait(1000)
    log.info("bt.send", "Hello I'm Luat BLE")
    while true do
        _, bt_recv = sys.waitUntil("BT_DATA_IND") --等待接收到数据
        local data = ""
        local len = 0
        local uuid = ""
        while true do
            local recvuuid, recvdata, recvlen = btcore.recv(3)
            if recvlen == 0 then
                break
            end
            uuid = recvuuid
            len = len + recvlen
            data = data .. recvdata
        end
        if len ~= 0 then
            log.info("bt.recv_data", data)
            log.info("bt.recv_data len", len)
            log.info("bt.recv_uuid", string.toHex(uuid))
            if data == "close" then
                btcore.disconnect()--主动断开连接
                if btWifiTdmTest then return end
            end
            btcore.send(data, 0xfee2, bt_connect.handle)
        end
    end
end

local ble_test = {init, poweron,data_trans}

sys.taskInit(function()
    sys.wait(5000)
    for _, f in ipairs(ble_test) do
        f()
    end
end)




