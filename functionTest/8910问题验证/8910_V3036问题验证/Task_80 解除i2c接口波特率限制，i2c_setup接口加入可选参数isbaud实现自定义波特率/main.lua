PROJECT = "I2C-gw"
VERSION = "1.0.0"

--加载日志功能模块，并且设置日志输出等级
--如果关闭调用log模块接口输出的日志，等级设置为log.LOG_SILENT即可
require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE

require "sys"

require "utils"

sys.taskInit(function()
    --初始化i2c接口
    sys.wait(5000)
    local result = i2c.setup(2,380000,-1,1)
    log.info("i2c PCF8563 initial",result)
    sys.wait(5000)

    while true do
        --发2个字节命令
        local sent = i2c.send(2,0x51,0x00)
        log.info("i2c PCF8563 sent",sent)

        --等一等器件
        sys.wait(5)

        --读结果
        local recv = i2c.recv(2,0x51,8)
        log.info("i2c PCF8563 recv hex",recv:toHex())

        -- local sent = i2c.send(2,0x51,0x01)
        -- log.info("i2c PCF8563 sent",sent)

        -- --等一等器件
        -- sys.wait(5)

        -- --读结果
        -- local recv = i2c.recv(2,0x51,1)
        -- log.info("i2c PCF8563 register01 recv hex",recv:toHex())

        -- local sent = i2c.send(2,0x51,0x02)
        -- log.info("i2c PCF8563 sent",sent)

        -- --等一等器件
        -- sys.wait(5)

        -- --读结果
        -- local recv = i2c.recv(2,0x51,1)
        -- log.info("i2c PCF8563 register02 recv hex",recv:toHex())

        -- local sent = i2c.send(2,0x51,0x03)
        -- log.info("i2c PCF8563 sent",sent)

        -- --等一等器件
        -- sys.wait(5)

        -- --读结果
        -- local recv = i2c.recv(2,0x51,1)
        -- log.info("i2c PCF8563 register03 recv hex",recv:toHex())
        
        sys.wait(500)
    end
end)


--启动系统框架
sys.init(0, 0)
sys.run()