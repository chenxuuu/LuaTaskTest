module(...,package.seeall)

require "pins"

-- 关闭防抖功能
-- pio.pin.setdebounce(0xffffffff)
-- -- flmt: io15/14
-- pio.pin.setdir(pio.INT, pio.P0_15, pio.CONT)
-- pio.pin.setdir(pio.INT, pio.P0_14, pio.CONT)
-- sys.taskInit(
--     function()
--         while true do
--             local ret = {
--                 flmt1 = pio.pin.getval(pio.P0_15),
--                 flmt2 = pio.pin.getval(pio.P0_14)
--             }
--             if ret ~= nil and type(ret) == 'table' then
--                 log.info('ret: ', json.encode(ret))
--             end
--             sys.wait(1000)
--         end
--     end
-- )
pio.pin.setdebounce(0xffffffff)
pio.pin.setdir(pio.INT,pio.P0_5,pio.CONT)
pio.pin.setdir(pio.INT,pio.P0_17,pio.CONT)
sys.taskInit(
    function()
        while true do
            local ret = {
                flmt1 = pio.pin.getval(pio.P0_5),
                flmt2 = pio.pin.getval(pio.P0_17)
            }
            if ret ~= nil and type(ret) == 'table' then
                log.info('ret: ',json.encode(ret))
            end
            sys.wait(1000)
        end
    end
)