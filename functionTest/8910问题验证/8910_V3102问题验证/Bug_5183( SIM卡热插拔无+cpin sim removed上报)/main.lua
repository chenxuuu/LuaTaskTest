PROJECT = "SIM_RESET_TEST"
VERSION = "1.0.0"

require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE

require "netLed"
pmd.ldoset(2, pmd.LDO_VLCD)
netLed.setup(true, pio.P0_1, pio.P0_4)

require "sys"
require "pins"
require "net"

local function gpioIntHandle(msg)
    log.info("testGpioSingle.gpio7IntFnc",msg,getGpio23Fnc())
    if msg == cpu.INT_GPIO_POSEDGE then
        log.info("SIM_RESET------------", "拔卡")
        --前面1：sim1,后面参数0：拔卡，1：插卡
        rtos.notify_sim_detect(1, 0)
    else
        log.info("SIM_RESET------------", "插卡")
        rtos.notify_sim_detect(1, 1)
    end
end

getGpio23Fnc = pins.setup(pio.P0_23, gpioIntHandle, pio.PULLDOWM)

--启动系统框架
sys.init(0, 0)
sys.run()
