PROJECT = "sensor"
VERSION = "1.0.0"

require "log"
require "sys"

i2cid = 2
i2cslaveaddr = 0x7E

sys.taskInit(function()
    sys.wait(8000)
    local speed = 1000
    if i2c.setup(i2cid, speed, -1, 1) ~= speed then
        log.error("i2c", "i2c.setup fail")
        return
    end
    while true do
        sys.wait(500)
        i2c.send(i2cid, i2cslaveaddr, 0x7E)
    end
end)

sys.init(0, 0)
sys.run()

