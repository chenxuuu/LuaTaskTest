module(..., package.seeall)

local slen = string.len
local waitTime = 10000

--- 流式sm3算法测试
-- @return 无
-- @usage flowMd5Test()
local function flowSm3Test()
    local sm3Obj=crypto.sm3start()
    local testTable={"abc"}
    for i=1, #(testTable) do
       log.info( sm3Obj:sm3update(testTable[i]) )
    end
    log.info("testCrypto.flowSm3Test",sm3Obj:sm3finish())
end

sys.taskInit(
    function ()
        while true do
            sys.wait(waitTime)
            flowSm3Test()
        end
    end
)
