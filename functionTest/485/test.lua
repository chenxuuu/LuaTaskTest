-- 模块功能：硬件485测试程序.
-- @author guowen

require "utils"
require "pm"
require "pins"
--require "nvm"
require "misc"

module(...,package.seeall)

local runFlg = true

local function taskLed(ledPinSetFunc)
	while true do
        local data = string.char(0x01,0x03,0x00,0x2A,0x00,0x01,0xA5,0xC2)
        uart.write(1, data)
        log.info(">", string.toHex(data))
		sys.wait(200)
        data = string.char(0x02,0x03,0x00,0x2A,0x00,0x01,0xA5,0xF1)
        uart.write(1, data)
        log.info(">", string.toHex(data))
        sys.wait(200)
	end
end

--处理串口数据
local function proc1(data)
    if #data~=7 then
        log.error("********************")
    end
    log.info("<", data, "  length：" , string.len(data))
    rtos.sleep(300)
    uart.write(1,data)
end


--接收串口数据
local function read1()
    local data = ""
    --底层core中，串口收到数据时：
    --如果接收缓冲区为空，则会以中断方式通知Lua脚本收到了新数据；
    --如果接收缓冲器不为空，则不会通知Lua脚本
    --所以Lua脚本中收到中断读串口数据时，每次都要把接收缓冲区中的数据全部读出，
    --这样才能保证底层core中的新数据中断上来，此read函数中的while语句中就保证了这一点
    while true do
        data = uart.read(1, "*l")
        --log.info("data =",data)
        --数据不存在时停止接收数据
        if not data or string.len(data) == 0 then break end
        --真正的串口数据处理函数
        proc1(data)
    end
end

function init()
	--保持系统处于唤醒状态，不会休眠
	pm.wake("test")
    uart.setup(1, 115200, 8, uart.PAR_NONE, uart.STOP_1, nil, 1);
    uart.set_rs485_oe(1, pio.P0_7)
	uart.on(1, "receive", read1)
    --sys.taskInit(taskLed)
end


init()